package buswifi

import grails.converters.JSON
import grails.gsp.PageRenderer
import grails.plugins.rest.client.RestBuilder
import org.codehaus.groovy.grails.web.json.*
import static groovyx.net.http.Method.*
import static groovyx.net.http.ContentType.JSON

class HomeController {

	def rest = new RestBuilder()

	//String url = "http://192.168.1.38/adsAPI/rest"
	//String userUrl = "http://192.168.1.38/buswifi/rest"

	String url = "http://203.150.67.26:8080/adsAPIDev/rest"
	String userUrl = "http://203.150.67.26:8080/buswifi/rest"

	def authUsername = "admin"
	def authPassword = "admin"

	def authString = authUsername+':'+authPassword
	def encoded = authString.bytes.encodeBase64().toString()
    def authHeader = "Basic "+encoded

	static defaultAction = "libary"

	def beforeInterceptor = [
		action: this.&checkAuthen,
		execpt: ['login', 'authen']
	]

	def checkAuthen() {

		if(!session.user) {
			render(view:"login")
		}
	}	

	def authen() {
		
		String username  = params.username
		String password  = params.password

		def serviceUrl = url+"/userapi/verify"

		try {
			def resp = rest.post(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					userName = username
					userPassword = password
				}
			}		

			JSONObject data = new JSONObject(resp.json)

			if(resp.getStatus() == 200 && !data.error){
				session.user = data
		        render(contentType: "text/json"){[
		            'status' : 200
		        ]}
			}
			else{
				println data
		        render(contentType: "text/json"){[
		            'status' : 401
		        ]}
			}
			
		} catch(e) {
			println e.toString();
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}			
		}

	}  
 
	def login(){ 
	}

	def logout(){
		session.user = null
		render(view : "login")
	} 

    def libary() {
    	def criteria = "?sort=id&order=desc&max=10&page=1"
    	def serviceUrl = url+"/mediaapi/getmedias"+criteria
		List medias
		def Total
		def Sort
		def Order
		def Max
		def Page
		def PageAmount 

		try {

			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
	
			if(resp.getStatus() == 200 && !data.error){
				Total = data.total
				Sort = data.sort
				Order = data.order
				Max = data.max
				Page = data.page
				medias = data.mediaList
			}
			else{
				flash.error = "Internal server error." 
			}

		} catch(e) {
			println e.toString()
			flash.error = "Internal server error." 	
		}

		[activeMenu : 'media', medias : medias, total : Total, sort : Sort, order : Order, max : Max, page : Page]
    	
    }

    def getMedias() {
    	def paramSort
    	def paramOrder
    	if(params.sort){ 
    		paramSort = params.sort 
    		paramOrder = params.order
    	}
    	else {  
    		paramSort = 'id'
    		paramOrder = 'desc'
    	}
    	def criteria = "?sort="+paramSort+"&order="+paramOrder+"&max="+params.max+"&page="+params.page
    	def serviceUrl = url+"/mediaapi/getmedias"+criteria

		List medias
		def Total
		def Sort
		def Order
		def Max
		def Page
		def PageAmount 

		try {

			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)

			if(resp.getStatus() == 200 && !data.error){
				Total = data.total
				Sort = data.sort
				Order = data.order
				Max = data.max
				Page = data.page
				medias = data.mediaList
			}
			else{
				flash.error = "Internal server error." 
			}

		} catch(e) {
			println e.toString()
			flash.error = "Internal server error." 	
		}

		render (view: 'ajax/libary', model : [activeMenu : 'media', medias : medias, total : Total, sort : Sort, order : Order, max : Max, page : Page])
    	
    }

    def newmedia() {
    	[activeMenu : 'media']
    }

    def addMedia() {

    	def serviceUrl = url+"/mediaapi"
    	def jsonParams = JSON.parse(params.medias)
    	def result = false
    	
    	jsonParams.each(){ media ->
			try {
				def resp = rest.post(serviceUrl){
					contentType "application/json"
					header "Authorization", authHeader
					json {
						name = media.name
						desc = media.desc
						path = media.path
						idUser = media.idUser
						idMediaType = media.idMediaType
						url = media.url
					}
				}		
 
				JSONObject data = new JSONObject(resp.json)

				if(resp.getStatus() == 200 && !data.error){
			        result = true
				}
				else{
					println data
			        render(contentType: "text/json"){[
			            'status' : 500
			        ]}
				}
				
			} catch(e) {
				println e.toString();
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			}

    	}

	    if(result == true){
	        render(contentType: "text/json"){[
	            'status' : 200
	        ]}	    		
    	}
			
    } 

    def updateMedia() {
    	def serviceUrl = url+"/mediaapi/update"

    	def Id = params.id
    	def Name = params.name
    	def Desc = params.desc
    	def Path = params.path
    	def User = params.user
    	def Type = params.type==1?1:1
    	def Url  = params.url

    	try{ 

			def resp = rest.post(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
				json {
					id = Id
					name = Name
					desc = Desc
					path = Path
					idUser = User
					idMediaType = Type
					url = Url
				}
			}	 


			if(resp.getStatus() == 200){
		        render(contentType: "text/json"){[
		            'status' : 200
		        ]}
			}
			else{
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}
			}

    	} catch(e) {
			println e.toString();
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}				
		}

    }

    def deleteMedia(){
    	def serviceUrl = url+"/mediaapi/del"
    	def ids = params.list('mediaId[]')
    	def result = false

    	ids.each(){ mediaId ->

	    	try{ 

				def resp = rest.post(serviceUrl){
					contentType "application/json"
					header "Authorization", authHeader
					json {
						id = mediaId
					}
				}	 

				if(resp.getStatus() == 200){
					result = true
				}
				else{
			        render(contentType: "text/json"){[
			            'status' : 500
			        ]}	
				}

	    	} catch(e) {
				println e.toString();
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}	
			} 

    	}

    	if(result == true){
	        render(contentType: "text/json"){[
	            'status' : 200
	        ]}	    		
    	}

    }

    def schedule() {

    	def serviceUrlMedias = url+"/mediaapi/getmedias"
    	def serviceUrlPlaylists = url+"/playlistapi/getplaylists"
    	List medias
    	List playlists

		try {
			def resp = rest.get(serviceUrlMedias){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)

			if(resp.getStatus() == 200 && !data.error){

				medias = data.mediaList

			}
			else{
				flash.error = "Internal server error." 
			}

		} catch(e) {
			println e.toString()
			flash.error = "Internal server error." 		
		}


		try {
			def resp = rest.get(serviceUrlPlaylists){
				contentType "application/json"
				header "Authorization", authHeader
			}
			if(resp.getStatus() == 200 && resp.json instanceof JSONArray){
				JSONArray data = new JSONArray(resp.json)
				playlists = data
			}
			else{
				playlists = [] 
			}

		} catch(e) {
			println e.toString()
			flash.error = "Internal server error." 		
		} 


    	[activeMenu : 'schedule', medias : medias, playlists : playlists]
    }

    def addPlaylist(){

    	def serviceUrlCreatePlaylist = url+"/playlistapi"
    	def Name = params.name
    	def Desc = params.desc
    	def IdUser = params.idUser
    	def MediaIds = params.list('mediaIds[]')

    	try {
			def resp = rest.post(serviceUrlCreatePlaylist){   		
				contentType "application/json"
				header "Authorization", authHeader
				json {
					name = Name
					desc = Desc
					idUser = IdUser 
					mediaIds = MediaIds
				}							
    		}
			JSONObject data = new JSONObject(resp.json)
 
			if(resp.getStatus() == 200 && !data.error){
		        render(contentType: "text/json"){[
		            'status' : 200,
		            'id' : data.id,
		            'name' : data.name
		        ]}
			}
			else{
				println data
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			}     		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		}

    }

    def getPlaylist(){
    	def serviceUrl = url+"/playlistapi/getplaylist?id="+params.id

		try {
			def resp = rest.get(serviceUrl){   		
				contentType "application/json"
				header "Authorization", authHeader
    		}

			JSONObject data = new JSONObject(resp.json)

			if(resp.getStatus() == 200 && !data.error){
		        render(contentType: "text/json"){[
		            'status' : 200,
		            'playlist' : data 
		        ]}	
			}
			else{
				println data
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			} 
    		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	 
		}    	 
    }

    def deletePlaylist(){
    	def serviceUrl = url+"/playlistapi/del"
    	def Id = params.id
     	try {
			def resp = rest.post(serviceUrl){   		
				contentType "application/json"
				header "Authorization", authHeader
				json {
					id = Id
				}							
    		}

			if(resp.getStatus() == 200){

				JSONArray data = new JSONArray(resp.json)
		        render(contentType: "text/json"){[
		            'status' : 200,
		            'schedule' : data
		        ]}
			}
			else{
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			}     		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		}   	
    }

	def updatePlaylist(){
    	def serviceUrlCreatePlaylist = url+"/playlistapi/update"
    	def Id = params.id
    	def Name = params.name
    	def Desc = params.desc
    	def MediaIds = params.list('mediaIds[]')

    	try {
			def resp = rest.post(serviceUrlCreatePlaylist){   		
				contentType "application/json"
				header "Authorization", authHeader
				json {
					id = Id
					name = Name
					desc = Desc
					mediaIds = MediaIds
				}							
    		} 
    		
			if(resp.getStatus() == 200){

				JSONArray data = new JSONArray(resp.json)
		        render(contentType: "text/json"){[
		            'status' : 200,
		            'schedule' : data
		        ]}
			}
			else{
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			}      		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		}		
	}    

    def user() {

		def serviceUrl = userUrl+"/userapi/getusers?sort=createDate&order=desc&max=10&page=1"

		List userList
		def Total
		def Sort
		def Order
		def Max
		def Page
		def PageAmount 

		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
			if(resp.getStatus() == 200 && !data.error){
				Total = data.total
				Sort = data.sort
				Order = data.order
				Max = data.max
				Page = data.page
				userList = data.userList
			}
			else{
				flash.error = "Internal server error." 
			}

		} catch(e) {
			println e.toString()
			flash.error = "Internal server error." 		
		} 

    	[activeMenu : 'user', users : userList, total : Total, sort : Sort, order : Order, max : Max, page : Page]
    }

    def getUsers() { 

    	def paramSort
    	def paramOrder
    	println params.sort
    	if(params.sort){ 
    		paramSort = params.sort 
    		paramOrder = params.order
    	}
    	else { 
    		paramSort = 'createDate'
    		paramOrder = 'desc'
    	}

		def serviceUrl = userUrl+"/userapi/getusers?sort="+paramSort+"&order="+paramOrder+"&max="+params.max+"&page="+params.page+"&start="+params.startTime+"&end="+params.endTime
		println serviceUrl
		List userList
		def Total
		def Sort
		def Order
		def Max
		def Page
		def PageAmount	
		def StartTime
		def EndTime

		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
			if(resp.getStatus() == 200 && !data.error){
				Total = data.total
				Sort = data.sort
				Order = data.order
				Max = data.max
				Page = data.page
				userList = data.userList	
				StartTime = data.startTime
				EndTime = data.endTime	

			}
			else{
				flash.error = "Internal server error." 
			}

		} catch(e) { 
			println e.toString()
			flash.error = "Internal server error." 		
		} 
		render (view : 'ajax/user', model : [activeMenu : 'user', users : userList, total : Total, sort : Sort, order : Order, max : Max, page : Page, startTime : StartTime, endTime : EndTime, showStart : new Date(StartTime), showEnd : new Date(EndTime) ])
    	
    }

    def searchUsers() { 

		def serviceUrl = userUrl+"/userapi/search?mobileNo="+params.search
		println serviceUrl
		
		def User
		def Result	

		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
			if(resp.getStatus() == 200 && !data.error){
				User = data
				if(resp.body == "No user found"){
					Result = "No user found"
				}
			}
			else{
				flash.error = "Internal server error." 
			}

		} catch(e) { 
			println e.toString()
			flash.error = "Internal server error." 		
		} 
		render (view : 'ajax/searchUser', model : [activeMenu : 'user', user : User, result : Result])
    	
    }

	def resetUser(){
    	def serviceUrl = userUrl+"/userapi/resetuser"

    	def MobileNo = params.mobileNo
    	try {
			def resp = rest.post(serviceUrl){   		
				contentType "application/json"
				header "Authorization", authHeader
				json {
					mobileNo = MobileNo
				}							
    		} 

			if(resp.getStatus() == 200){
		        render(contentType: "text/json"){[
		            'status' : 200
		        ]}					
			}
			else{
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			}      		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		}		
	}      

    def userFilter() { 

		def serviceUrl = userUrl+"/userapi/getusers?sort=createDate&order=desc&max=10&page=1&start="+params.startTime+"&end="+params.endTime
		println serviceUrl
		List userList
		def Total
		def Sort
		def Order
		def Max
		def Page
		def StartTime
		def EndTime


		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)

			if(resp.getStatus() == 200 && !data.error){
				Total = data.total
				Sort = data.sort
				Order = data.order
				Max = data.max
				Page = data.page
				userList = data.userList
				StartTime = data.startTime
				EndTime = data.endTime			 		
			}
			else{
				println data
				flash.error = "Internal server error." 
			}

		} catch(e) { 
			println e.toString()
			flash.error = "Internal server error." 		
		} 
		render (view : 'ajax/user', model : [activeMenu : 'user', users : userList, total : Total, sort : Sort, order : Order, max : Max, page : Page, startTime : StartTime, endTime : EndTime, showStart : new Date(StartTime), showEnd : new Date(EndTime) ])
    	
    }        

    def getSchedules() { 
		def serviceUrl = url+"/scheduleapi/getschedules"
		try {
			def resp = rest.get(serviceUrl){   		
				contentType "application/json"
				header "Authorization", authHeader
    		}

			if(resp.getStatus() == 200 && resp.json instanceof JSONArray){
				JSONArray data = new JSONArray(resp.json)		

				render(contentType: "text/json"){[
					'schedules' : data,
					'status' : 200

		        ]}	
			}
			else{
				JSONObject data = new JSONObject(resp.json)
		        render(contentType: "text/json"){[
		            'status' : 200,
		            'schedules' : []
		        ]}	
			}
    		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	 
		} 

    }

    def getSchedule() { 
		def serviceUrl = url+"/scheduleapi/getschedule?id="+params.id

		try {
			def resp = rest.get(serviceUrl){   		
				contentType "application/json"
				header "Authorization", authHeader
    		}

			JSONObject data = new JSONObject(resp.json)

			if(resp.getStatus() == 200 && !data.error){
		        render(contentType: "text/json"){[
		            'status' : 200,
		            'schedule' : data 
		        ]}	
			}
			else{
				println data
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			} 
    		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	 
		} 

    }    

    def addSchedule() {

		def serviceUrl = url+"/scheduleapi"

    	def IdPlaylist = params.idPlaylist
    	def StartDate = params.startDate
    	def EndDate = params.endDate
    	def IdUser = params.idUser
    	
    	try {
			def resp = rest.post(serviceUrl){   		
				contentType "application/json"
				header "Authorization", authHeader
				json {
					idPlaylist = IdPlaylist
					startDate = StartDate
					endDate = EndDate
					idUser = IdUser 
				}							
    		}  
 
			JSONObject data = new JSONObject(resp.json)

			if(resp.getStatus() == 200 && !data.error){
		        render(contentType: "text/json"){[
		            'status' : 200,
		            'schedule' : data
		        ]}	
			}
			else{
				println data
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			}    		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		}	 
    }

    def updateSchedule() {
    	def serviceUrl = url+"/scheduleapi/update"

    	def Id = params.id
    	def IdPlaylist = params.idPlaylist
    	def Start = params.startDate
    	def End = params.endDate
    	println Start
    	try {
			def resp = rest.post(serviceUrl){   		
				contentType "application/json"
				header "Authorization", authHeader
				json {
					id = Id
					idPlaylist = IdPlaylist
					startDate = Start
					endDate = End
				}							
    		} 
 
			if(resp.getStatus() == 200){
		        render(contentType: "text/json"){[
		            'status' : 200
		        ]}
			}
			else{
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			}     		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		}	
    }

    def deleteSchedule() {
    	def serviceUrl = url+"/scheduleapi/del"
    	def Id = params.id
 
     	try {
			def resp = rest.post(serviceUrl){   		
				contentType "application/json"
				header "Authorization", authHeader
				json {
					id = Id
				}							
    		}

			if(resp.getStatus() == 200){
		        render(contentType: "text/json"){[
		            'status' : 200
		        ]}
			}
			else{
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}				
			}     		
    	} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		}     	
    }

    def report(){

    	def criteria = "?sort=requestTime&order=desc&max=10&page=1&type=2"
    	def serviceUrl = url+"/medialogapi/getlogs"+criteria

		List mediaList
		def Total
		def Sort
		def Order
		def Max
		def Page
		def PageAmount 

		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
			if(resp.getStatus() == 200 && !data.error){
				Total = data.total
				Sort = data.sort
				Order = data.order
				Max = data.max
				Page = data.page
				mediaList = data.logList
			}
			else{
				flash.error = "Internal server error." 
			}
 
		} catch(e) {
			println e.toString()
			flash.error = "Internal server error." 		
		} 

		[activeMenu : 'report', medias : mediaList, total : Total, sort : Sort, order : Order, max : Max, page : Page]    	
    } 

    def getReport(){
    	def paramType = params.type ? params.type : "2"
    	def paramSort
    	def paramOrder
    	def startTime = params.startTime ? params.startTime : ""
    	def endTime = params.endTime ? params.endTime : ""
    	def requestTime = params.requestTime ? params.requestTime : ""

    	if(params.sort){ 
    		paramSort = params.sort 
    		paramOrder = params.order 
    	}
    	else { 
    		paramSort = 'requestTime'
    		paramOrder = 'desc'
    	}
    	def criteria = "?sort="+paramSort+"&order="+paramOrder+"&max="+params.max+"&page="+params.page+"&startTime="+startTime+"&endTime="+endTime+"&requestTime="+requestTime+"&type="+paramType
    	def serviceUrl = url+"/medialogapi/getlogs"+criteria
    	println serviceUrl
		List mediaList
		def Total
		def Sort
		def Order
		def Max
		def Page
		def StartTime
		def EndTime
		def RequestTime 

		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
			if(resp.getStatus() == 200 && !data.error){
				Total = data.total
				Sort = data.sort
				Order = data.order
				Max = data.max
				Page = data.page
				StartTime = data.startTime
				EndTime = data.endTime
				RequestTime = data.requestTime
				mediaList = data.logList		

			}
			else{
				flash.error = "Internal server error." 
			}
 
		} catch(e) {
			println e.toString()
			flash.error = "Internal server error." 		
		} 

		render (view: 'ajax/report', model : [activeMenu : 'report', medias : mediaList, total : Total, sort : Sort, order : Order, max : Max, page : Page, startTime : StartTime, endTime : EndTime, requestTime : RequestTime, unsuccess : paramType ])

    }

    PageRenderer groovyPageRenderer

    def mediaFilter(){

    	def paramType = params.type ? params.type : "2"
    	def paramSort
    	def paramOrder 
    	if(params.sort){ 
    		paramSort = params.sort 
    		paramOrder = params.order
    	}
    	else { 
    		paramSort = 'startTime'
    		paramOrder = 'desc'
    	}
    	def criteria = "?sort="+paramSort+"&order="+paramOrder+"&max="+params.max+"&page="+params.page+"&startTime="+params.startTime+"&endTime="+params.endTime+"&type="+paramType
    	def serviceUrl = url+"/medialogapi/getlogs"+criteria
    	println serviceUrl
		List mediaList
		def html 
		def Total
		def Sort
		def Order
		def Max
		def Page		
		def Sum
		def StartTime
		def EndTime
		def RequestTime 

		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			JSONObject data = new JSONObject(resp.json)
			if(resp.getStatus() == 200 && !data.error){

				Total = data.total
				mediaList = data.logList 
				Sort = data.sort
				Order = data.order
				Max = data.max
				Page = data.page				
				Sum = data.summary
				StartTime = data.startTime
				EndTime = data.endTime
				RequestTime = data.requestTime
				html =  groovyPageRenderer.render(view: '/home/ajax/report', model : [activeMenu : 'report', medias : mediaList, total : Total, sort : Sort, order : Order, max : Max, page : Page, startTime : StartTime, endTime : EndTime, requestTime : RequestTime, unsuccess : paramType ])

		        render(contentType: "text/json"){[
		            'status' : 200,
		            'summary' : Sum,
		            'medias' : mediaList,
		            'total' : Total, 
		            'startTime' : StartTime,
		            'endTime' : EndTime,
		            'html' : html
		        ]}						
			}
			else{
		        render(contentType: "text/json"){[
		            'status' : 500
		        ]}	
			} 
 
		} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		} 

    }

    def mediaExport(){

    	def paramType = params.type ? params.type : "2"
    	def criteria = "?startTime="+params.startTime+"&endTime="+params.endTime+"&type="+paramType
    	def serviceUrl = url+"/medialogapi/export"+criteria
    	println serviceUrl
		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			
	        render(contentType: "text/json"){[
	            'status' : 200,
	            'url' : resp.body
	        ]}
  
		} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		} 

    }

    def userExport(){

    	def criteria = "?start="+params.startTime+"&end="+params.endTime
    	def serviceUrl = userUrl+"/userapi/export"+criteria
    	println serviceUrl
		try {
			def resp = rest.get(serviceUrl){
				contentType "application/json"
				header "Authorization", authHeader
			}
			
	        render(contentType: "text/json"){[
	            'status' : 200,
	            'url' : resp.body
	        ]}
 
		} catch(e) {
			println e.toString()
	        render(contentType: "text/json"){[
	            'status' : 500
	        ]}	
		} 

    }          


}
