$( ".content-report" ).ready( function() {

	initReportPaging();
	datePickerReport();
	reportNumeric();
	checkSuccessChecked();
});

function filterReport(){
	if($('#filter-media-form').length){
		$('#filter-media-form').validate({

			rules: {
				'filterDateStart' : {
					required : true
				},
				'filterDateEnd' : {
					required : true
				}							
			},
            errorPlacement: function(error, element) { },			
			highlight: function(element){
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			submitHandler: function(form){
				if(!$('#unsuccess').is(':checked') && !$('#success').is(':checked')){
	                $('.checkSuccess-error').removeClass('hide');					
				}else{
					$('.checkSuccess-error').addClass('hide');	
					$('.section-item-report-detail').html('');
					submitMediaFilter();	
				}
			}
		});
	}	
}

function submitMediaFilter(){

	var Max = $('#maxSelect').val();
	var Sort = $('#hiddenSort').val();
	var Order = $('#hiddenOrder').val();

	var IncludeUnsuccess = '';

	if($('#success').is(':checked') && $('#unsuccess').is(':checked') ){
		IncludeUnsuccess = '2';
	}else if( $('#success').is(':checked') && !$('#unsuccess').is(':checked')){
		IncludeUnsuccess = '0';
	}else{
		IncludeUnsuccess = '1';
	}

	var startDate = moment($('#filterDateStart').val()).format('YYYY-MM-DD');
	var endDate =  moment($('#filterDateEnd').val()).format('YYYY-MM-DD');

	var startH = $('#filterHourStart').val() ? $('#filterHourStart').val() : '00';
	var startM = $('#filterMinStart').val() ? $('#filterMinStart').val() : '00';
	var startS = $('#filterSecStart').val() ? $('#filterSecStart').val() : '00';
	var endH = $('#filterHourEnd').val() ? $('#filterHourEnd').val() : '00';
	var endM = $('#filterMinEnd').val() ? $('#filterMinEnd').val() : '00';
	var endS = $('#filterSecEnd').val() ? $('#filterSecEnd').val() : '00';

	var startTime = startH+':'+startM+':'+startS;
	var endTime = endH+':'+endM+':'+endS;

	if(endTime == '00:00:00'){
		endTime = '23:59:59';
	}

	var start = startDate+' '+startTime;
	var end = endDate+' '+endTime;
	var convertedStart = moment(start).valueOf();
	var convertedEnd = moment(end).valueOf();

	$.ajax({
		type : 'POST',
		url : '/mediaFilter',
		data : {
			startTime : convertedStart,
			endTime : convertedEnd,
			sort : Sort,
			max : Max,
			page : 1,
			order : Order,
			type : IncludeUnsuccess
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){

			if(data.status == 200){

				if(data.total > 0){
					//renderContentAfterDoughnut(convertedStart, convertedEnd ,IncludeUnsuccess);

					//draw table
					$('.content-main').html('');
					$('.content-main').html(data.html);
					initReportPaging();
					//draw table

					$('.total-view').html(data.total).digits();
					$('.reportStart').html(start);
					$('.reportEnd').html(end);

						var sumArr = [];
						_.mapObject(data.summary, function(val, key){

							sumArr.push({
								value : val,
								label : key
							});

						});			
						$('.chart-report').show();
						drawDoughnut(sumArr);
						drawDetailReport(sumArr, data.total);
						checkScrollbar();
						scrollToTop();

				}else{
					$('.chart-report').hide();
					var txt = '<span class="noResultReport">Have no result. <a href="/report"> Click here to return.</a></span>';
					$('.content-main').html(txt);
				}
			
			}
			else{
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                setTimeout(function(){window.location='/report';} , 2000);     				
			}

		},
		complete : function(){
			$('#unsuccess').attr('checked', false);
			$('#modal-loading').modal('hide');
			$('#modal-filter').find('input').each(function(){
				$(this).val('');
			});
			$('#modal-filter').modal('hide');
		}
	});
}

function drawDoughnut(sumArr){

	var Template = $('.doughnut');

	var clone = Template.clone(true, true);

	$('.pie-report').html('');
	$('.pie-report').html(clone);

	data = getLogData(sumArr);

	var options = {
		percentageInnerCutout : 65
	};

	var ctx = $("#mediaLogChart").get(0).getContext("2d");	

	var myDoughnutChart = new Chart(ctx).Doughnut(data,options);
}

var getLogData = function(sumArr){

	for(var i in sumArr){
		_.extend(sumArr[i], { color : colorReport(i)});
	}

	return sumArr;
}

function drawDetailReport(sumArr, total){

	_.each(sumArr, function(data){
		$('.section-item-report-detail').append(initDetailReport(data.label, data.value, data.color, total));
	});

}

var initDetailReport = function(key, val, color, total){

	var percentage = parseFloat((parseInt(val)/parseInt(total))*100).toFixed(2);
	var row = '<div class="item-report">'+
                '<div class="log-item" style="border-color:'+color+'">'+
                '<div class="log-name">'+key+'</div>'+
                '<div class="log-cal">'+percentage+'% ('+commaSeparateNumber(val)+' VIEWS)</div> '+
            	'</div></div>';

    return row;
}

var colorReport = function(index){

	if(index <= 40){
		var color = ['#ff5f5f', '#463c5f', '#6ee1f5', '#fff666', '#75ff75', 
		'#98579a', '#508ddf', '#deddf7', '#ad671e', '#451031',
		'#eba184', '#c7d5c1', '#749866', '#8a6698', '#60476a',  
		'#ff6a6a', '#3300ff', '#fff9c7', '#42d4b0', '#f9ca6f',
		'#aaaaaa', '#887ab8', '#2dbdfc', '#eeadb5', '#5ec39d',
		'#aadfff', '#ffb1e0', '#fffa84', '#cba2ff', '#5c2007',
		'#e3ff50', '#294e81', '#2f3828', '#80c95b', '#fffb99',
		'#5ec39d', '#bbbbbb', '#d84a4a', '#dbc396', '#357bc9',
		];
		return color[index];		
	}

	else {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
		color += letters[Math.round(Math.random() * 15)];
		}
		return color;
	}

}

function reportNumeric(){
	$('#filterSecStart').numeric();
	$('#filterMinStart').numeric();	
	$('#filterHourStart').numeric();	
	$('#filterSecEnd').numeric();	
	$('#filterMinEnd').numeric();	
	$('#filterHourEnd').numeric();	

}

function datePickerReport(){
	$('.ui-datepicker').unwrap();
    $( "#filterDateStart" ).datepicker({ 
        dateFormat: 'dd/MM/yy',onSelect: function(selected) {
            $("#filterDateEnd").datepicker("option","minDate", selected)
        }

    });
    $( "#filterDateEnd" ).datepicker({ 
        dateFormat: 'dd/MM/yy',onSelect: function(selected) {
            $("#filterDateStart").datepicker("option","maxDate", selected)
        }
    });	
    $('.ui-datepicker').wrap('<div class="datepicker ll-skin-latoja"></div>');	

    $('#filterDateStart').keyup(function(){
        $(this).val('');
    });
    $('#filterDateEnd').keyup(function(){
        $(this).val('');        
    });       
}

function reportPaging(Page){

	var Max = $('#maxSelect').val();
	var Sort = $('#hiddenSort').val();
	var Order = $('#hiddenOrder').val();
	var start = $('#hiddenStartTime').val();
	var end = $('#hiddenEndTime').val();		
	var request = $('#hiddenRequestTime').val();
	var unsuccess = $('#hiddenUnsuccess').val() ? $('#hiddenUnsuccess').val() : '2';

	$.ajax({
		type : 'GET',
		url : '/getReport',
		data : {
			sort : Sort,
			max : Max,
			page : Page,
			order : Order,
			startTime : start,
			endTime : end,
			requestTime : request,
			type : unsuccess	
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html('');
			$('.content-main').html(data);
			initReportPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function initReportPaging(){
	var Total = ($('#hiddenTotal').val() /$('#hiddenMax').val());
	var Page = $('#hiddenPage').val();

	$('#report-selection').bootpag({
	    total: Math.ceil(Total),
	    page : Page,
	    maxVisible : 10
	}).on('page', function(event, num){
		reportPaging(num);
	});

	filterReport();	
	datePickerReport();
	reportNumeric();
	initDropDownReport();
}

function reportCriteria(){
	var Max = $('#maxSelect').val();
	var Sort = $('#hiddenSort').val();	
	var Order = $('#hiddenOrder').val();
	var start = $('#hiddenStartTime').val();
	var end = $('#hiddenEndTime').val();
	var request = $('#hiddenRequestTime').val();		
	var unsuccess = $('#hiddenUnsuccess').val() ? $('#hiddenUnsuccess').val() : '2';

	$.ajax({
		type : 'GET',
		url : '/getReport',
		data : {
			sort : Sort,
			max : Max,
			order : Order,
			page : 1,
			startTime : start,
			endTime : end,
			requestTime : request,
			type : unsuccess				
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html();
			$('.content-main').html(data);
			initReportPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function reportSort(Sort, Order){

	var Max = $('#maxSelect').val();
	var Page = $('#hiddenPage').val();
	var start = $('#hiddenStartTime').val();
	var end = $('#hiddenEndTime').val();
	var request = $('#hiddenRequestTime').val();	
	var unsuccess = $('#hiddenUnsuccess').val() ? $('#hiddenUnsuccess').val() : '2';

	$.ajax({
		type : 'GET',
		url : '/getReport',
		data : {
			sort : Sort,
			max : Max,
			order : Order,
			page : Page,
			startTime : start,
			endTime : end,
			requestTime : request,
			type : unsuccess					
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html();
			$('.content-main').html(data);
			initReportPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function resetReportField(){

	$('.txtTime').each(function(){
		$(this).select2('val', '00');
	});
	$('#success').prop( "checked", true );
	$('#unsuccess').prop( "checked", true );

	$('#filterDateStart').val('');	
	$('#filterDateEnd').val('');	

    $('#filterDateStart').datepicker( "option" , {
    minDate: null,
    maxDate: null} );
    $('#filterDateEnd').datepicker( "option" , {
    minDate: null,
    maxDate: null} );

}

$('#modal-filter').on('hidden.bs.modal', function () {
    resetReportField();
});

function renderContentAfterDoughnut(start, end, IncludeUnsuccess){
	$('.content-main').html('');
	$.ajax({
		type : 'GET',
		url : '/getReport',
		data : {
			max : 10,
			page : 1,
			startTime : start,
			endTime : end,
			type : IncludeUnsuccess
		},
		success : function(data){
			$('.content-main').html(data);
			initReportPaging();
		}
	});	
}

function exportMediaLog(filtered){

	if(filtered === 0){
		submitExportMediaLog('', '');
	}else{
		var start = $('#hiddenStartTime').val();
		var end = $('#hiddenEndTime').val();
		submitExportMediaLog(start, end);
	}

}

function submitExportMediaLog(start, end){

	var unsuccess = $('#hiddenUnsuccess').val() ? $('#hiddenUnsuccess').val() : '2';

	$.ajax({
		type : 'POST',
		url : '/mediaExport',
		data : {
			startTime : start,
			endTime : end,
			type : unsuccess	
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
			window.location = data.url;
		}		
	});
}

function initDropDownReport(){
	$('.txtTime').select2({
	    minimumResultsForSearch: Infinity
	});

	$('.select2-container--default').each(function(){
		$(this).removeAttr("style").css('width', '40px');
	});	
}

function checkScrollbar(){

	var h = $('#item-report').height();
	if( h >= 270){
		$('.section-item-report-detail').addClass('item-report-scrollbar');
	}

}

$('#modal-filter').on('hidden.bs.modal', function () {
   resetReportField();
});

function checkSuccessChecked(){
	if(!$('#unsuccess').is(':checked') && !$('#success').is(':checked')){
		$('#success').prop( "checked", true );
	}
}

$('#success').click(function(){
	if($(this).is(':checked')){
		$('.checkSuccess-error').addClass('hide');
	}
});
$('#unsuccess').click(function(){
	if($(this).is(':checked')){
		$('.checkSuccess-error').addClass('hide');
	}
});
