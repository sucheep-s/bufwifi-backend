// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery.min
//= require jquery-ui.min
//= require underscore-min
//= require bootstrap
//= require jquery.validate.min
//= require jquery.numeric.min
//= require jquery.bootpag.min
//= require moment.min
//= require fullcalendar
//= require gcal
//= require chart.js
//= require select2
//= require script
//= require schedule
//= require report

if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}
