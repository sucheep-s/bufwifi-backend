$.fn.digits = function(){ 
    return this.each(function(){ 
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}
function commaSeparateNumber(val){
	while (/(\d+)(\d{3})/.test(val.toString())){
	  val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
	}
	return val;
}
$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip(); 
	$('#media-num').numeric({ negative : false, decimal : false });

	if($('#form-num').length){
		$('#form-num').validate({

			rules: {
				'mediaNum' : {
					required : true,
					minlength : 1,
					maxlength : 20 
				}
			},
			messages : {
				mediaNum : '*'
			},
			highlight: function(element){
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			submitHandler: function(form){
				/*
				if($('.media-field-active').length > 0){

					var current = $('.media-field-active').length;
					var newElement = $('#media-num').val();
					var total =parseInt(current)+parseInt(newElement);

					if( total > 20){
						$('.form-group-num').addClass('has-error');
						$('.add-media-warning').addClass('add-media-error');
						return false;
					}
					else{
						$('.form-group-num').removeClass('has-error');
						$('.add-media-warning').removeClass('add-media-error');
						addMediaElement();	
					}
				}else{
					addMediaElement();	
				}*/
				addMediaElement();	
			}
		});
	}	
});

var Template = $('#fieldSet');

function addMediaElement(){

	$('#alert-error').hide();

	var mediaNum = $('#media-num').val();
	
	if($('.media-field-active ').length >= 1 ){
		$('.media-field-active').remove();
	}

	for (var i = 1; i <= mediaNum; i++) {

		var clone = Template.clone(true, true);

		clone.attr('data-order', i);
		clone.attr('id', clone.attr('id') + i.toString() );
		clone.attr('class', 'media-field-active' );
	    clone.find('input, textarea').each(function( index ) {
	        var elem = $(this);
	        elem.attr('id', elem.attr('id') + i.toString());
	        elem.attr('name', elem.attr('name') + i.toString());
	        elem.attr('class', elem.attr('class') + '-clone');
	    });
	    clone.appendTo('.form-field').removeClass('hide');
		
	};	

    if(mediaNum >= 1){
    	$('.add-media-btn').show();
    }else{
    	$('.add-media-btn').hide();
    }

    //nextId++;
}


function addMedia(){

	$('.media-field-active').each(function(){

		if($(this).find('.mediaName-clone').val() == ''){

			$(this).find('.form-group-name').addClass('has-error');

		}

		if($(this).find('.redirectUrl-clone').val() == ''){

			$(this).find('.form-group-url').addClass('has-error');

		}

		if($(this).find('.pathFile-clone').val() == ''){

			$(this).find('.form-group-path').addClass('has-error');

		}

		if($(this).find('.mediaName-clone').val()){

			$(this).find('.form-group-name').removeClass('has-error');

		}

		if($(this).find('.redirectUrl-clone').val()){

			$(this).find('.form-group-url').removeClass('has-error');

		}		

		if($(this).find('.pathFile-clone').val()){

			$(this).find('.form-group-path').removeClass('has-error');

		}

	});

	if( $('.has-error').length > 0){
		$('#alert-error').show();
		scrollToTop();
	}
	else {
		$('#alert-error').hide();
		var instanceMedias = [];
		$('.media-field-active').each(function(){
			instanceMedias.push({
				name : $(this).find('.mediaName-clone').val(),
				desc : $(this).find('.desc-clone').val(),
				url  : $(this).find('.redirectUrl-clone').val(),	
				path : $(this).find('.pathFile-clone').val(),
				idUser : $('#userId').val(),
				idMediaType : 1
				//idMediaType : $(this).find('.fileType-clone').val()
			});
		});

		$.ajax({
			type : 'POST',
			url : '/addMedia',
		    data: {medias: JSON.stringify( instanceMedias )},
		    dataType: "json",
			beforeSend : function(){
				$('#modal-loading').modal('show');
			},
			success : function(data){

				setTimeout(function(){$('#modal-loading').modal('hide');},500);	

				if(data.status == 200){
					scrollToTop();
					$('#alert-success').show();
					setTimeout(function(){window.location='/libary'} , 1000);
				}
				else {
					$('#alert-success').hide();
					$('.text-error').html('Internal Server Error.');
					$('#alert-error').show();
					setTimeout(function(){window.location='/newmedia'} , 2000);
				}
			}

		});
	}

}

function updateMedia(id){

	var name = $('#txtName'+id).val();
	var desc = $('#txtDesc'+id).val();
	var type = $('#txtType'+id+' option:selected').val();
	var path = $('#txtPath'+id).val();
	var url = $('#txtUrl'+id).val();

	var typeText = $('#txtType'+id+' option:selected').text()

	if(name.length ==0){
		$('#txtName'+id).addClass('input-error').focus();
	}
	else if (url.length == 0){
		$('#txtUrl'+id).addClass('input-error').focus();
		$('#txtName'+id).removeClass('input-error');			
	}	
	else if(path.length == 0){
		$('#txtPath'+id).addClass('input-error').focus();
		$('#txtName'+id).removeClass('input-error');
		$('#txtUrl'+id).removeClass('input-error');
	}else{

		$.ajax({
			type : 'POST',
			url : '/updateMedia',
		    data: {
		    	'id' : id,
		    	'name' : name,
		    	'desc' : desc,
		    	'type' : type,
		    	'url' : url,
		    	'path' : path,
		    	'user' : $('#userId').val()
		    },	
			beforeSend : function(){
				$('#modal-loading').modal('show');
			},
			success : function(data){
				if(data.status == 200){

					$('#spanName'+id).html(name);
					$('#spanDesc'+id).html(desc);
					$('#spanType'+id).html(typeText);
					$('#spanUrl'+id).html(url);
					$('#spanPath'+id).html(path);

					$('#hiddenName'+id).val(name);
					$('#hiddenDesc'+id).val(desc);
					$('#hiddenUrl'+id).val(url);	
					$('#hiddenPath'+id).val(path);					

					$('#modal-loading').modal('hide');
					activeLibary($('#edit'+id));

				}else{
					$('#loading-state').hide();
					$('#loading-error').html('Internal server error').show();
					setTimeout(function(){window.location='/libary'} , 2000);
				}
			}
		});

	}

}

function modalDeleteMedia(){
    if($(".chk").is(":checked")){
		$('#del-media-modal').modal('show');
	}
}

function deleteMedia(){
	var id = [];
	var Page = $('#hiddenPage').val();
	$(".chk:checked").each(function() {
		id.push($(this).val());
	});

	$.ajax({
		type : 'POST',
		url : '/deleteMedia',
		data : { mediaId : id },
		beforeSend : function(){
		    $('#del-media-modal').modal('hide');
		    $('#modal-loading').modal('show');
		},
		success : function(data){
		    setTimeout(function(){$('#modal-loading').modal('hide');},500);
		    /*_.each(id, function(Id){
		    	$('#'+Id).fadeOut().remove();
		    });*/
		    mediaPaging(Page);	    
		}
	});

}


function scrollToTop() {
	$('html, body').animate({scrollTop : 0},500);
	return false;
}

function activeLibary(element){

	var id = $(element).attr("data-id");
	var tr =  $('#'+id);

	if(!$(element).hasClass('active')){

		$('.row-media').find('input, .cancel, .save').each(function(){
			$(this).addClass('hide');
		});
		$('.row-media').find('span').each(function(){
			$(this).removeClass('hide');
		});
		$('.edit').removeClass('active');

		$('#cancel'+id).removeClass('hide');
		$('#save'+id).removeClass('hide');
		$(element).addClass('active');
		tr.find('input').each(function(){
	        $(this).removeClass('hide');
		});
		tr.find('span').each(function(){
	        $(this).addClass('hide');
		});	
	}
	else{
		$('#cancel'+id).addClass('hide');
		$('#save'+id).addClass('hide');
		$(element).removeClass('active');
		tr.find('input').each(function(){
	        $(this).addClass('hide');
		});
		tr.find('span').each(function(){
	        $(this).removeClass('hide');
		});				
	}
		$('.chk').removeClass('hide');
}

function checkMediaAll(element){
    if(element.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;                        
        });
    }
    else{
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = false;                        
        });
    }	
}
function cancelEditMedia(id){

	var name = $('#txtName'+id).val();
	var desc = $('#txtDesc'+id).val();
	var type = $('#txtType'+id+' option:selected').val();
	var path = $('#txtPath'+id).val();

}


/************************************************USER**********************************************************************/

function userCriteria(){
	var Max = $('#maxSelect').val();
	var Sort = $('#hiddenSort').val();	
	var Order = $('#hiddenOrder').val();	
	var start = $('#hiddenStartTime').val() ? $('#hiddenStartTime').val() : '';
	var end = $('#hiddenEndTime').val() ? $('#hiddenEndTime').val() : '';		

	$.ajax({
		type : 'GET',
		url : '/getUsers',
		data : {
			sort : Sort,
			max : Max,
			page : 1,
			order : Order,
			startTime : start,
			endTime : end
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html('');
			$('.content-main').html(data);
			initUserPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function userSort(Sort, Order){
	var Max = $('#maxSelect').val();
	var Page = $('#hiddenPage').val();
	var start = $('#hiddenStartTime').val() ? $('#hiddenStartTime').val() : '';
	var end = $('#hiddenEndTime').val() ? $('#hiddenEndTime').val() : '';	

	$.ajax({
		type : 'GET',
		url : '/getUsers',
		data : {
			sort : Sort,
			max : Max,
			order : Order,
			page : Page,
			startTime : start,
			endTime : end			
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html('');
			$('.content-main').html(data);
			initUserPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function userPaging(Page){
	var Max = $('#maxSelect').val();
	var Sort = $('#hiddenSort').val();
	var Order = $('#hiddenOrder').val();
	var start = $('#hiddenStartTime').val() ? $('#hiddenStartTime').val() : '';
	var end = $('#hiddenEndTime').val() ? $('#hiddenEndTime').val() : '';	

	$.ajax({
		type : 'GET',
		url : '/getUsers',
		data : {
			sort : Sort,
			max : Max,
			page : Page,
			order : Order,
			startTime : start,
			endTime : end				
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html('');
			$('.content-main').html(data);
			initUserPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function initUserPaging(){
	var Total = ($('#hiddenTotal').val() /$('#hiddenMax').val());
	var Page = $('#hiddenPage').val();

	$('#user-selection').bootpag({
	    total: Math.ceil(Total),
	    page : Page,
	    maxVisible : 10
	}).on('page', function(event, num){
		userPaging(num);
	});	

	filterUser();
	datePickerExportUser();
	exportUserNumeric();
	initDropDownUserExport();
	searchUser();
}
/************************************************USER**********************************************************************/


/************************************************media**********************************************************************/
function mediaCriteria(){
	var Max = $('#maxSelect').val();
	var Sort = $('#sortSelect').val();	
	var Order = $('#hiddenOrder').val();
	
	$.ajax({
		type : 'GET',
		url : '/getMedias',
		data : {
			sort : Sort,
			max : Max,
			order : Order,
			page : 1
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html();
			$('.content-main').html(data);
			initMediaPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function mediaSort(Sort, Order){

	var Max = $('#maxSelect').val();
	var Page = $('#hiddenPage').val();

	$.ajax({
		type : 'GET',
		url : '/getMedias',
		data : {
			sort : Sort,
			max : Max,
			order : Order,
			page : Page
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html();
			$('.content-main').html(data);
			initMediaPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function mediaPaging(Page){

	var Max = $('#maxSelect').val();
	var Sort = $('#hiddenSort').val();
	var Order = $('#hiddenOrder').val();

	$.ajax({
		type : 'GET',
		url : '/getMedias',
		data : {
			sort : Sort,
			max : Max,
			page : Page,
			order : Order
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			$('.content-main').html();
			$('.content-main').html(data);
			initMediaPaging();
		},
		complete : function(){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
		}
	});
}

function initMediaPaging(){
	var Total = ($('#hiddenTotal').val() /$('#hiddenMax').val());
	var Page = $('#hiddenPage').val();

	$('#media-selection').bootpag({
	    total: Math.ceil(Total),
	    page : Page,
	    maxVisible : 10
	}).on('page', function(event, num){
		mediaPaging(num);
	});	
}

function cancelActive(element){

	var id = $(element).attr("data-id");
	var name = $('#hiddenName'+id).val();
	var desc = $('#hiddenDesc'+id).val();
	var path = $('#hiddenPath'+id).val();
	var url = $('#hiddenUrl'+id).val();

	$('#txtName'+id).val(name);
	$('#txtName'+id).removeClass('input-error');
	$('#txtDesc'+id).val(desc);
	$('#txtPath'+id).val(path);
	$('#txtPath'+id).removeClass('input-error');
	$('#txtUrl'+id).val(url);
	$('#txtUrl'+id).removeClass('input-error');	

}
/************************************************media**********************************************************************/

$( ".content-libary" ).ready( function() {
  initMediaPaging();
});

$( ".content-user" ).ready( function() {
  initUserPaging();
});


/*******************************************Export User***********************************************************************/
function datePickerExportUser(){
	$('.ui-datepicker').unwrap();
    $( "#userExportDateStart" ).datepicker({ 
        dateFormat: 'dd/MM/yy',
        onSelect: function(selected) {
            $("#userExportDateEnd").datepicker("option","minDate", selected)
        }

    });
    $( "#userExportDateEnd" ).datepicker({ 
        dateFormat: 'dd/MM/yy',
        onSelect: function(selected) {
            $("#userExportDateStart").datepicker("option","maxDate", selected)
        }
    });	
    $('.ui-datepicker').wrap('<div class="datepicker ll-skin-latoja"></div>');

    $('#userExportDateStart').keyup(function(){
        $(this).val('');
    });
    $('#userExportDateEnd').keyup(function(){
        $(this).val('');        
    });    	
}

function exportUserNumeric(){

	$('#userExportSecStart').numeric();
	$('#userExportMinStart').numeric();	
	$('#userExportHourStart').numeric();	
	$('#userExportSecEnd').numeric();	
	$('#userExportMinEnd').numeric();	
	$('#userExportHourEnd').numeric();	

}
function filterUser(){
	if($('#filter-user-form').length){
		$('#filter-user-form').validate({

			rules: {
				'userExportDateStart' : {
					required : true
				},
				'userExportDateEnd' : {
					required : true
				}						
			},
            errorPlacement: function(error, element) { },			
			highlight: function(element){
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			submitHandler: function(form){
				submitUserFilter();	
			}
		});
	}	
}

function submitUserFilter(){

	var Max = $('#maxSelect').val();
	var Sort = $('#hiddenSort').val();
	var Order = $('#hiddenOrder').val();	

	var startDate = moment($('#userExportDateStart').val()).format('YYYY-MM-DD');
	var endDate =  moment($('#userExportDateEnd').val()).format('YYYY-MM-DD');

	var startH = $('#userExportHourStart').val() ? $('#userExportHourStart').val() : '00';
	var startM = $('#userExportMinStart').val() ? $('#userExportMinStart').val() : '00';
	var startS = $('#userExportSecStart').val() ? $('#userExportSecStart').val() : '00';
	var endH = $('#userExportHourEnd').val() ? $('#userExportHourEnd').val() : '00';
	var endM = $('#userExportMinEnd').val() ? $('#userExportMinEnd').val() : '00';
	var endS = $('#userExportSecEnd').val() ? $('#userExportSecEnd').val() : '00';

	var startTime = startH+':'+startM+':'+startS;
	var endTime = endH+':'+endM+':'+endS;

	if(endTime == '00:00:00'){
		endTime = '23:59:59';
	}

	var start = startDate+' '+startTime;
	var end = endDate+' '+endTime;
	var convertedStart = moment(start).valueOf();
	var convertedEnd = moment(end).valueOf();

	$.ajax({
		type : 'POST',
		url : '/userFilter',
		data : {
			startTime : convertedStart,
			endTime : convertedEnd
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){

			$('.content-main').html('');
			$('.content-main').html(data);
			initUserPaging();

		},
		complete : function(){
			$('#modal-loading').modal('hide');
			$('#modal-user-filter').find('input').each(function(){
				$(this).val('');
			});
			$('#modal-user-filter').modal('hide');
		}
	});
}

function exportUser(filtered){

	if(filtered === 0){
		submitExportUser('', '');
	}else{
		var start = $('#hiddenStartTime').val();
		var end = $('#hiddenEndTime').val();
		submitExportUser(start, end);
	}

}

function submitExportUser(start, end){

	$.ajax({
		type : 'POST',
		url : '/userExport',
		data : {
			startTime : start,
			endTime : end
		},
		beforeSend : function(){
			$('#modal-loading').modal('show');
		},
		success : function(data){
			setTimeout(function(){$('#modal-loading').modal('hide');},500);
			window.location = data.url;
		}		
	});
}
function initDropDownUserExport(){

	$('.txtTime').select2({
	    minimumResultsForSearch: Infinity
	});

	$('.select2-container--default').each(function(){
		$(this).removeAttr("style").css('width', '40px');
	});	

}

function resetUserExportField(){

	$('.txtTime').each(function(){
		$(this).select2('val', '00');
	});

	$('#userExportDateStart').val('');	
	$('#userExportDateEnd').val('');	

    $('#userExportDateStart').datepicker( "option" , {
    minDate: null,
    maxDate: null} );
    $('#userExportDateEnd').datepicker( "option" , {
    minDate: null,
    maxDate: null} );

}
$('#modal-user-filter').on('hidden.bs.modal', function () {
    resetUserExportField();
});

function submitSearchUser(mobile){
	$.ajax({
		type: 'POST',
		url: '/searchUsers',
		data: {
			search : mobile
		},
		beforeSend: function(){
			$('#modal-loading').modal('show');
		},
		success: function(data){
			$('#modal-loading').modal('hide');

			$('.content-main').html('');
			$('.content-main').html(data);
			$('.text-header').html('Search Result');
		}
	});
}

$( ".content-user" ).ready( function() {

	searchUser();

});

function searchUser(){

	$('#input-search').numeric({ negative : false, decimal : false });

	if($('#form-search-user').length){
		$('#form-search-user').validate({
			rules: {
				'search' : {
					required : true,
					minlength : 10
				}							
			},
	        errorPlacement: function(error, element) { },			
			highlight: function(element){
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			submitHandler: function(form){
				submitSearchUser($('#input-search').val());
			}
		});
	}

}

function resetUserPassword(mobile){

	$.ajax({
		type: 'POST',
		url: '/resetUser',
		data : {
			mobileNo : mobile
		},
		beforeSend: function(){
			$('#modal-user-reset').modal('hide');
			$('#modal-loading').modal('show');
		},
		success: function(data){

			if(data.status == 200){

				$('#loading-state').hide();
				$('#loading-success').show();
				$('#loading-success').html('SMS was sent');
				$('.smsSent').removeClass('hide');		
			}

		},
		complete : function(){
			setTimeout(function(){
				$('#modal-loading').modal('hide');
				$('#loading-state').show();
				$('#loading-success').hide();						
			} , 4000);				
		}
	});
}