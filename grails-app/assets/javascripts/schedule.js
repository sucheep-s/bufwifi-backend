var calendarEvent = {};

$(document)
.on('show.bs.modal', '.modal', function(event) {
    $(this).appendTo($('body'));
})
.on('shown.bs.modal', '.modal.in', function(event) {
    setModalsAndBackdropsOrder();
})
.on('hidden.bs.modal', '.modal', function(event) {
    setModalsAndBackdropsOrder();
});
$('.modal').on('hide.bs.modal', function () {
    $('body').removeAttr( 'style' );
})

$('.content-schedule').ready(function() {


    /////////////////////////////////Date picker field/////////////////////////
    $('.txtStart').keyup(function(){
        $(this).val('');
    });
    $('.txtEnd').keyup(function(){
        $(this).val('');        
    });    

    //////////////////////////////playlist add scrollbar////////////////////
    generatePlaylistScrollbar();
    //////////////////////////////playlist color//////////////////////////////
   // generatePlaylistColor();
    //////////////////////////////playlist color//////////////////////////////


    //////////////////////////////datepicker//////////////////////////////

    $( ".txtStart" ).datepicker({ 
        dateFormat: 'dd/MM/yy',
        minDate: new Date(),
        onSelect: function(selected) {
            $(".txtEnd").datepicker("option","minDate", selected)
        }

    });
    $( ".txtEnd" ).datepicker({ 
        dateFormat: 'dd/MM/yy',
        onSelect: function(selected) {
            $(".txtStart").datepicker("option","maxDate", selected)
        }
    });

    //.attr('readonly','readonly');  
    $('.ui-datepicker').wrap('<div class="datepicker ll-skin-latoja"></div>');
    //////////////////////////////datepicker//////////////////////////////


    //////////////////////////////Calendar//////////////////////////////
    if ($('#external-events div.external-event').length) {
        genDragElement();
        draggableCalendar();        
    }//end #external-events div.external-event
    else{
        $('#calendar').fullCalendar({
            header: {
                left: 'prev, next',
                center : 'title',
                right: 'month'
            }    
        });       
    }
    //////////////////////////////Calendar//////////////////////////////


    //////////////////////////////create playlist//////////////////////////////

    if($('#form-create-playlist').length){
        $('#form-create-playlist').validate({

            rules: {
                'input-create-playlist' : {
                    required : true
                }
            },
            errorPlacement: function(error, element) { },
            highlight: function(element){
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            submitHandler : function(form){
                var isAddSchedule = true;
                if($('#txtCreateStart').val() || $('#txtCreateEnd').val()){

                    if(!$('#txtCreateStart').val()){
                        $('#form-create-startDate').addClass('has-error');
                    }else if( !$('#txtCreateEnd').val()){
                        $('#form-create-endDate').addClass('has-error');
                    }
                    else{
                        addPlaylist(isAddSchedule);
                    }

                }else {
                    isAddSchedule = false;
                    addPlaylist(isAddSchedule);
                }                     
                
            }
        });
    }       
    //////////////////////////////create playlist//////////////////////////////

    //////////////////////////////update playlist//////////////////////////////
    if($('#form-edit-playlist').length){
        $('#form-edit-playlist').validate({
            rules: {
                'input-edit-playlist' : {
                    required : true
                }
            },
            errorPlacement: function(error, element) { },
            highlight: function(element){
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            submitHandler : function(form){
                $('.noMedia-playlist').remove();
                var isAddSchedule = true;
                if($('#txtEditStart').val() || $('#txtEditEnd').val()){
                    if(!$('#txtEditStart').val()){
                        $('#form-edit-startDate').addClass('has-error');
                    }else if( !$('#txtEditEnd').val()){
                        $('#form-edit-endDate').addClass('has-error');
                    }
                    else{
                        updatePlaylist(isAddSchedule);
                    }

                }else {
                    isAddSchedule = false;
                    updatePlaylist(isAddSchedule);
                }                  
                
            }
        });
    }
    //////////////////////////////update playlist//////////////////////////////    

});

function datePicker(){

    $( ".txtStart" ).datepicker( "option" , {
    minDate: new Date(),
    maxDate: null} );
    $( ".txtEnd" ).datepicker( "option" , {
    minDate: null,
    maxDate: null} );

}

function setModalsAndBackdropsOrder() {
  var modalZIndex = 1040;
  $('.modal.in').each(function(index) {
    var $modal = $(this);
    modalZIndex++;
    $modal.css('zIndex', modalZIndex);
    $modal.next('.modal-backdrop.in').addClass('hidden').css('zIndex', modalZIndex - 1);
});
  $('.modal.in:visible:last').focus().next('.modal-backdrop.in').removeClass('hidden');
}

function generatePlaylistScrollbar(){
    $('#external-events').removeClass('scroll-playlist');
    if($('#external-events').height() >= 660 ){
        $('#external-events').addClass('scroll-playlist');
    }
    if($('#external-events').height() < 660 ){
        $('#external-events').removeClass('scroll-playlist');
    }   
}

function genDragElement(){

    $('#external-events div.external-event').each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
            title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        dragging(this);

    });

}
function draggableCalendar(){

    var date = new Date();
    var dd = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    getSchedules();

    $('#calendar').fullCalendar({
        header: {
            left: 'prev, next',
            center : 'title',
            right: 'month'
        },
        eventLimit: true, // for all non-agenda views     
        eventLimitText: "Something",
        droppable: true, // this allows things to be dropped onto the calendar !!!
        drop: function (date, allDay) { // this function is called when something is dropped
            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            //$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
            var id =  $.trim($(this).find('.hiddenPlaylist').val());
            var title = copiedEventObject.title;
            var convertedDate = moment(date).format('YYYY-MM-DD');
            addSchedule(id, title, convertedDate, convertedDate);

        },
        eventClick:  function(event, jsEvent, view) {
            //set the values and open the modal
            calendarEvent = event;
            getSchedule(event.id);
        }       
    });//end full calendar    
}

/* 
function generatePlaylistColor(){
   
    var playlistAmount = $('.external-event').length;
    var run = 1;
    for(var i = 1; i<=playlistAmount; i++){
      if(run==1){ $('#external-events > div:nth-child('+i+') >span').attr('data-q', '1'); }
      else if (run==2){$('#external-events > div:nth-child('+i+') >span').attr('data-q', '2'); }
      else if (run == 3){$('#external-events > div:nth-child('+i+') >span').attr('data-q', '3'); }
      else if (run == 4){$('#external-events > div:nth-child('+i+') >span').attr('data-q', '4'); }
      else { $('#external-events > div:nth-child('+i+') >span').attr('data-q', '5'); }
      run++;
      if(run > 5){ run=1; }
    }

    $('*[data-q="1"]').addClass('btn-info');
    $('*[data-q="2"]').addClass('btn-success');
    $('*[data-q="3"]').addClass('btn-primary');
    $('*[data-q="4"]').addClass('btn-danger');
    $('*[data-q="5"]').addClass('btn-warning');    
}*/

function dragging(element){

    $(element).draggable({
        //helper: 'clone',

        helper: function(){
            return $(this).clone().width($(this).width());
        },       
        zIndex: 999,
        revert: true, // will cause the event to go back to its
        revertDuration: 0 //  original position after the drag
    }).click(function(){
        if ( $(element).is('.ui-draggable-dragging') ) {
            return;
        }
        var id = $(element).find('input').val();
        modalPlaylist(id);
    });     

}

function addPlaylist(isAddSchedule){
    var startDate = moment($('#txtCreateStart').val()).format('YYYY-MM-DD');
    var endDate = moment($('#txtCreateEnd').val()).format('YYYY-MM-DD');
        if($('.playlist-created-row').length ==0){
            $('#body-create-row').html(errorPlaylist());
        }
        else{

            var medias = [];
            $('.playlist-created-row').each(function(){
                medias.push($(this).find('.hiddenMediaId').val());
            });
            $.ajax({
                type : 'POST',
                url : '/addPlaylist',
                data : {
                    name : $('#input-create-playlist').val(),
                    desc : $('#input-create-desc').val(),
                    idUser : $('#userId').val(),
                    mediaIds : medias
                },
                beforeSend : function(){
                    $('#modal-create-schedule').modal('hide');
                    $('#modal-loading').modal('show');
                },
                success : function(data){

                    if(data.status == 200){
                        appendPlaylist(data.id, data.name);
                        if(isAddSchedule){addScheduleFromPlaylist(data.id, data.name, startDate, endDate); } 
                        else { $('#modal-loading').modal('hide'); } 

                        $('#calendar').fullCalendar( 'destroy' );
                        setTimeout(function(){draggableCalendar();} , 100);      
                                                  
                    }else {
                        $('.text-error').html('Internal Server Error.');
                        $('#alert-error').show();
                        setTimeout(function(){window.location='/schedule';} , 2000);                        
                    }
                },
                complete : function(){
                    //if($('.external-event').length <= 1){
                    //    draggableCalendar(); 
                    //}
                    datePicker();
                }
            });
        } 
}
var errorPlaylist = function(){
    var text = '<tr class="error-playlist"><td>Please add media.</td></tr>';
    return text;
}
var noMedia = function(){
    var text = '<tr class="noMedia-playlist"><td>Have no media in playlist.</td></tr>';
    return text;
}
function updatePlaylist(isAddSchedule){
    var startDate = moment($('#txtEditStart').val()).format('YYYY-MM-DD');
    var endDate = moment($('#txtEditEnd').val()).format('YYYY-MM-DD');    
        if($('.playlist-edit-row').length ==0){
            $('#body-edit-row').html(errorPlaylist());
        }
        else{
            var Id = $('#hiddenPlaylistId').val();
            var Name = $('#input-edit-playlist').val();
            var medias = [];
            $('.playlist-edit-row').each(function(){
                medias.push($(this).find('.hiddenEditMediaId').val());
            });
            $.ajax({
                type : 'POST',
                url : '/updatePlaylist',
                data : {
                    id   : Id,
                    name : Name,
                    desc : $('#input-edit-desc').val(),
                    mediaIds : medias
                },
                beforeSend : function(){
                    $('#modal-edit-schedule').modal('hide');
                    $('#modal-loading').modal('show');
                },
                success : function(data){
                    if(data.status == 200){
                        if(isAddSchedule){addScheduleFromPlaylist(Id, Name, startDate, endDate);}
                        //else{
                            $('#calendar').fullCalendar( 'destroy' );
                            setTimeout(function(){draggableCalendar();} , 100);                            
                        //}

                        $('#loading-state').hide();
                        $('#loading-success').html('Updating success.').show();                         
                    }else {
                        $('.text-error').html('Internal Server Error.');
                        $('#alert-error').show();
                        setTimeout(function(){window.location='/schedule';} , 2000);                        
                    }
                    
                },
                complete : function(){
                    setTimeout(function(){$('#modal-loading').modal('hide');} , 400);  
                    $('#loading-success').hide();
                    $('#loading-state').show();    
                    $('#event'+Id).find('span').html(Name);
                    datePicker();
                    //$('#content-wrapper').width('100%');
                }
            });
        }     
}
function removeMediaInPlaylist(){
    $('.mediaChk:checked').each(function(){
        $(this).parents('tr:first').remove();
    }); 
    sumMedia(); 
}
function refreshMediaInPlaylist(){
    var id = $('#hiddenPlaylistId').val();
    $('.playlist-edit-row').remove();
    $('.error-playlist').remove();
    modalPlaylist(id);
}
var preRenderEditPlaylist = function(id, name, type, url){
    var eType = '';
    if(type ==1){ eType='<td class="tr-playlist-type"><div class="td-type-position"><i class="glyphicon glyphicon-film"></i></div></td>';}
    else{ eType = '<td class="tr-playlist-type"><div class="td-type-position"><i class="glyphicon glyphicon-picture"></i></div></td>';}

    var row = '<tr id="edited-row'+id+'" class="playlist-edit-row playlist-row" onmouseover="addOrdering(this)" onmouseleave="removeOrdering(this)">'+
            '<td class="tr-playlist-chk"><input class="mediaChk" type="checkbox" value="'+id+'"></td>'+
            eType+
            '<td class="tr-playlist-name">'+name+'</td>'+
            '<td class="tr-playlist-url">'+url+'</td>'+
            '<td class="hide"><input type="hidden" class="hiddenEditMediaId" value="'+id+'"></td>'+           
            '</tr>';

    return row;        
}
var preRenderCreatePlaylist = function(id, name, type, url){
    var eType = '';
    if(type ==1){ eType='<td class="tr-playlist-type"><div class="td-type-position"><i class="glyphicon glyphicon-film"></i></div></td>';}
    else{ eType = '<td class="tr-playlist-type"><div class="td-type-position"><i class="glyphicon glyphicon-picture"></i></div></td>';}

    var row = '<tr id="created-row'+id+'" class="playlist-created-row playlist-row"  onmouseover="addOrdering(this)" onmouseleave="removeOrdering(this)">'+
            '<td class="tr-playlist-chk"><input class="mediaChk" type="checkbox" value="'+id+'"></td>'+
            eType+
            '<td class="tr-playlist-name">'+name+'</td>'+
            '<td class="tr-playlist-url">'+url+'</td>'+
            '<td class="hide"><input type="hidden" class="hiddenMediaId" value="'+id+'"></td>'+
            '</tr>';
    return row;        
}

function sumMedia(){
    $('.sum-media').html($('.table-playlist > tbody > .playlist-row').length);  
}
function sumCreatedMedia(){
    $('.sum-media').html($('.table-playlist > tbody > playlist-created-row').length);  
}

var preRenderOrdering = function(){
    var td = '<td class="tr-order"><a onclick="orderDown(this)"><i class="glyphicon glyphicon-triangle-bottom"></i></a>'+
             '<a onclick="orderUp(this)"><i class="glyphicon glyphicon-triangle-top"></i></a></td>';
    return td;
}
function addOrdering(element){
    var td = preRenderOrdering();
    if($('.tr-order').length == 0){$(element).find('.tr-playlist-type').before(td);}
}
function removeOrdering(element){
    $(element).find('.tr-order').remove();
}
function orderDown(element){
    var row = $(element).parents('tr:first');
    row.insertAfter(row.next());        
}
function orderUp(element){
    var row = $(element).parents('tr:first');
    row.insertBefore(row.prev());          
}
function appendPlaylist(id, name){
    var element = '<div class="external-event ui-draggable" id="event'+id+'"><span class="btn btn-sm btn-block">'+name+'</span><input type="hidden"/ class="hiddenPlaylist" value="'+id+'" /></div>';
    $('#external-events').append(element).fadeIn('slow');
    dragging($('#event'+id));
    //generatePlaylistColor();
    generatePlaylistScrollbar();
}

function checkPlaylist(){
    if($('.playlist-created-row').length ==0 ){
        $('#body-create-row').append(errorPlaylist());
    } 
    else {
        $('.error-playlist').remove();
    }
}

function addToCreatePlaylist(id, name, type, url){
    $('.error-playlist').remove();
    var row = preRenderCreatePlaylist(id, name, type, url);
    $('#body-create-row').append(row).fadeIn(1000);  
    sumMedia();  
}

function addToEditPlaylist(id, name, type, url){
    $('.error-playlist').remove();
    $('.noMedia-playlist').remove();
    var row = preRenderEditPlaylist(id, name, type, url); 
    $('#body-edit-row').append(row).fadeIn(1000);
    sumMedia();        
}

function renderEditPlaylist(id, name, type, url){
    var row = preRenderEditPlaylist(id, name, type, url); 
    $('#body-edit-row').append(row);   
}

$('#modal-create-schedule').on('hidden.bs.modal', function () {
    datePicker();
    $('.playlist-created-row').remove();
    $('.error-playlist').remove();
    $('.form-group').removeClass('has-error');
    $('#input-create-playlist').val('');
    $('#input-create-desc').val('');
    $('#txtCreateStart').val('');
    $('#txtCreateEnd').val(''); 

});

$('#modal-create-schedule').on('shown.bs.modal', function () {
    sumCreatedMedia();
});

function modalPlaylist(Id){
    $('#modal-edit-schedule').modal('show');

    $.ajax({
        type:'GET',
        url:'/getPlaylist',
        data: {id : Id},
        beforeSend : function(){
            $('#modal-loading').modal('show');
        },
        success : function(data){

            $('#hiddenPlaylistId').val(Id);
            if(data.status == 200){
                bindPlaylist(data.playlist.id, data.playlist.name, data.playlist.redirectUrl, data.playlist.desc, data.playlist.mediaList);
            }else{
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                setTimeout(function(){window.location='/schedule'} , 2000);                   
            }
            setTimeout(function(){$('#modal-loading').modal('hide');},400);
        },
        complete : function(){
            sumMedia();
            //checkHeightPlaylist();
        }

    });
}
$('#modal-edit-schedule').on('hidden.bs.modal', function () {
    datePicker();
    $('.playlist-edit-row').remove();
    $('.error-playlist').remove();
    $('.noMedia-playlist').remove();
    $('.form-group').removeClass('has-error');
    $('#input-edit-playlist').val('');
    $('#input-edit-desc').val('');
    $('#txtEditStart').val('');
    $('#txtEditEnd').val('');          
});

function bindPlaylist(id, name, redirectUrl, desc, mediaList){
    $('#hiddenPlaylistId').val(id);
    $('#input-edit-playlist').val(name);
    $('#input-edit-desc').val(desc);

    if(mediaList.length > 0){
        _.each(mediaList, function(list){
            renderEditPlaylist(list.id, list.name, list.idMediaType, list.url);
        });        
    }else{
        $('#body-edit-row').append(noMedia());
    }   

}

function deletePlaylist(){
    var Id = $('#hiddenPlaylistId').val();

    var element = $('#event'+Id);

    $.ajax({
        type : 'POST',
        url : '/deletePlaylist',
        data : {id : Id},
        beforeSend : function(){
            $('#del-playlist-modal').modal('hide');
            $('#modal-edit-schedule').modal('hide');
            $('#modal-loading').modal('show');
        },
        success : function(data){
            if(data.status == 200){
                _.each(data.schedule, function(e){
                    $('#calendar').fullCalendar('removeEvents', e); 
                });
                setTimeout(function(){$('#modal-loading').modal('hide');},400);
                element.fadeOut('slow').remove();
                //$('#calendar').fullCalendar( 'destroy' );
                //draggableCalendar(); 
            }else{
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                setTimeout(function(){window.location='/schedule'} , 2000);                   
            }
        },
        complete : function(){
            datePicker();
            generatePlaylistScrollbar();
            //checkHeightPlaylist();
            //generatePlaylistColor();
        }
    });
}

/********************************Search Media************************************/
$('#txtEditSearch').keyup(function() {
    searchMedia($('#tbody-edit-media'), $(this).val());
});
$('#txtCreateSearch').keyup(function() {
    searchMedia($('#tbody-create-media'), $(this).val());
});

function searchMedia(parent, filter){ 
    $(parent).find('tr').each(function(){
        if ($(this).find('.tr-media-name').text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut();
        } else {
            $(this).show();
        }
    });
}
/********************************Search Media************************************/

var eventColor = function(title){
    var color = '';
    var playlistElement = $(" .external-event > span").filter(function() { return ($(this).text() === title) });
    var order = $(playlistElement).data('q');
    if(order == 1){
        color = '#5bc0de';
    }else if(order == 2){
        color = '#5cb85c';
    }else if(order == 3){
        color = '#337ab7';
    }else if(order == 4){
        color = '#d9534f';
    }else{
        color = '#f0ad4e';
    }
    return color;
}

function getSchedules(){

    var Schedules = [];
    $.ajax({
        type : 'GET',
        url : '/getSchedules',
        success : function(data){
            if(data.status == 200){
                $('#modal-loading').modal('hide'); 
                _.each(data.schedules, function(schedule){
                    Schedules.push({
                        id : schedule.id,
                        title : schedule.playlistName,
                        start : schedule.startDate,
                        end : schedule.endDate,
                        color : '#5bc0de'
                    });
                });
                $('#calendar').fullCalendar('addEventSource', {events:Schedules}); 
            }
            else{
                $('#modal-loading').modal('show');               
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                setTimeout(function(){window.location='/schedule'} , 2000);                   
            }
        }
    });
}

function addSchedule(Id, Title, Start, End){
    var Schedules = [];
    $.ajax({
        type : 'POST',
        url : '/addSchedule',
        data : {
            idPlaylist : Id,
            startDate : Start,
            endDate : End,
            idUser : $('#userId').val()
        },
        beforeSend : function(){
            $("#modal-loading").modal("show");
        },
        success : function(data) {
            if(data.status == 200){
                $("#modal-loading").modal("hide");
                Schedules.push({
                    id : data.schedule.id,
                    title : data.schedule.playlistName,
                    start : data.schedule.startDate,
                    end : data.schedule.endDate,
                    color : '#5bc0de'
                });           
                $('#calendar').fullCalendar('addEventSource', {events:Schedules}); 
            }else{
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                setTimeout(function(){window.location='/schedule'} , 2000);  
            }
        }
    });
}

function addScheduleFromPlaylist(Id, Title, Start, End){
    var Schedule = [];
    $.ajax({
        type : 'POST',
        url : '/addSchedule',
        data : {
            idPlaylist : Id,
            startDate : Start,
            endDate : End,
            idUser : $('#userId').val()
        },
        success : function(data) {
            if(data.status == 200){
                Schedule.push({
                    id : data.schedule.id,
                    title : data.schedule.playlistName,
                    start : data.schedule.startDate,
                    end : data.schedule.endDate,
                    color : '#5bc0de'
                });                
                $('#calendar').fullCalendar('addEventSource', Schedule, false);
                $('#modal-loading').modal('hide');
            }else{
                $("#modal-loading").modal("show");
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                setTimeout(function(){window.location='/schedule'} , 2000);  
            }
        }
    });
}

function getSchedule(Id){
    $.ajax({
        type : 'GET',
        url : '/getSchedule',
        data : { id : Id},
        beforeSend : function(){
            $('#body-schedule-row').find('tr').remove();
        },
        success : function(data){
            if(data.status == 200){
                $("#modal-calendar").modal("show");
                bindSchedule(data.schedule.id, data.schedule.idPlaylist, data.schedule.playlistName, data.schedule.startDate, data.schedule.endDate);
            }else{
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                $('#modal-loading').modal('show');
                setTimeout(function(){window.location='/schedule'} , 2000);  
            }
        }
    });

}

function bindSchedule(Id, Playlist, Name, Start, End){
    getPlaylistSchedule(Playlist);
    $('#hiddenSchedule').val(Id);
    $('.header-modal-schedule').find('h4').html(Name);
    $('.txtStart').val(moment(Start).format('DD/MMMM/YYYY'));
    $('.txtEnd').val(moment(End).format('DD/MMMM/YYYY'));
}

function getPlaylistSchedule(Id){
    $.ajax({
        type:'GET',
        url:'/getPlaylist',
        data: {id : Id},
        success : function(data){
            if(data.status == 200){
                bindPlaylistSchedule(data.playlist.id, data.playlist.desc, data.playlist.mediaList);
                setTimeout(function(){$('#modal-loading').modal('hide');} , 400);   
             }else{
                $("#modal-loading").modal("show");
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                setTimeout(function(){window.location='/schedule'} , 2000);  
            }            
        }

    });
}
 
function bindPlaylistSchedule(id, desc, mediaList){
    $('#playlistIdSchedule').val(id);
    $('#desc-schedule').val(desc);
    _.each(mediaList, function(list){
        renderSchedulePlaylist(list.id, list.name, list.idMediaType, list.url);
    });     
}

function renderSchedulePlaylist(id, name, type, url){
    var eType = '';
    if(type ==1){ eType='<td class="tr-schedule-type"><i class="glyphicon glyphicon-film"></i></td>';}
    else{ eType = '<td class="tr-schedule-type"><i class="glyphicon glyphicon-picture"></i></td>';}

    var row = '<tr id="schedule-row'+id+'" class="playlist-edit-row playlist-row">'+
            eType+
            '<td class="tr-playlist-name">'+name+'</td>'+
            '<td class="tr-playlist-url">'+url+'</td>'+
            '</tr>';
    $('#body-schedule-row').append(row);   
    sumMedia();
}

$('#modal-calendar').on('hidden.bs.modal', function () {
    datePicker();
    $('.schedule-row').remove();
    $('.form-group').removeClass('has-error');
    $('.txtStart').val('');
    $('.txtEnd').val('');
    $('.playlist-edit-row').remove();
});

function deleteSchedule(){
    var Id = $('#hiddenSchedule').val();

    $.ajax({
        type : 'POST',
        url : '/deleteSchedule',
        data : {id : Id},
        beforeSend : function(){
            $('#del-schedule-modal').modal('hide');
            $('#modal-calendar').modal('hide');
            $('#modal-loading').modal('show');
        },
        success : function(data){
            if(data.status == 200){
                setTimeout(function(){$('#modal-loading').modal('hide');} , 400);   
                $('#calendar').fullCalendar('removeEvents', Id); 
            }else{
                $('#loading-state').hide();
                $('#loading-error').html('Internal server error').show(); 
                setTimeout(function(){window.location='/schedule';} , 2000);                   
            }
        }
    });
}

$('#form-schedule').submit(function(event){

    var Id = $('#hiddenSchedule').val();
    var IdPlaylist = $('#playlistIdSchedule').val();
    var Start = moment($('#txtScheduleStart').val()).format('YYYY-MM-DD');
    var End = moment($('#txtScheduleEnd').val()).format('YYYY-MM-DD');
    var Schedules = [];
   $.ajax({
        type : 'POST',
        url : '/updateSchedule',
        data : {
            id   : Id,
            idPlaylist : IdPlaylist,    
            startDate : Start,
            endDate : End
        },
        beforeSend : function(){
            $('#modal-calendar').modal('hide');
            $('#modal-loading').modal('show');
        },
        success : function(data){

            if(data.status == 200){
                $('#loading-state').hide();
                $('#loading-success').html('Updating success.').show(); 
                Schedules.push({
                    id : Id,
                    title : $('.header-modal-schedule').find('h4').text(),
                    start : Start,
                    end : End
                }); 

                $('#calendar').fullCalendar('removeEvents', Id); 
                $('#calendar').fullCalendar('addEventSource', {events:Schedules});  
                $('#modal-loading').modal('hide');                      
            }else {
                $('.text-error').html('Internal Server Error.');
                $('#alert-error').show();
                setTimeout(function(){window.location='/schedule';} , 2000);                        
            }
        },
        complete : function(){
            setTimeout(function(){$('#modal-loading').modal('hide');} , 2000);  
            $('#loading-success').hide();
            $('#loading-state').show();    
        }
    });
    return false;
});//end submit 
