$(function(){
	
	if($('#form-login').length){
		$('#form-login').validate({
			rules: {
				'username' : {
					required : true
				},
				'password' : {
					required : true
				}
			},
			messages : {
				username : {
					required : 'Please enter username.'
				},
				password : {
					required : 'Please enter password.'
				}
			},
			highlight: function(element) {
			    $(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		});		
	}


	$('#form-login').submit(function(event){
		$.ajax({
			type : 'POST',
			url : '/authen',
			data : $(this).serialize(),
			beforeSend : function(){
				$('#loading-login').show();
			},
			success : function(data){

				$('#loading-login').hide();	
				
				if(data.status == 200){
					window.location = "/libary";
				}else if(data.status == 401){
					$('#login-error').show();
				}else {
					$('#login-error').show().html('Internal server error.');
				}							
			}
		});//end ajax		
		return false;
	});//end submit*/


});