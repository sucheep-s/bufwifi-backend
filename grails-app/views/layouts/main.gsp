<!DOCTYPE html>
<html>
	<head>
		<link rel='shortcut icon' type='image/x-icon' href='/images/favicon.ico' />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>
		<g:layoutTitle default="ON WIFI" />
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
  		<asset:stylesheet src="application.css"/>
		<g:layoutHead/>
	</head>
	<body>

		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">

			    <div id="logo" class="navbar-header">
			      <img src="/images/ONwifi.png" width="47px" height="33px" />
			    </div>	
				<div id="admin-bar" class="fixed-right">
					<a><span>Hi, ${session.user.firstName}</span><img height="30" width="30" src="/images/profile.png"></a>
					<input type="hidden" id="userId" value="${session.user.id}"/>
		           <div class="admin-box">
		            <ul class="admin-box-user-actions">
		             <li>
		              <img src="/images/profile.png" class="avatar" height="70" width="75"><span class="display-name">${session.user.firstName}</span><br/><span class="display-position">Administrator</span>
		             </li>
		             <li>
		              <a class="display-logout" href="/logout">Log Out</a>  
		             </li>
		            </ul>
		           </div>            
				</div>	    

			</div><!--end container-fluid-->	
		</nav>

		<div id="wrapper">

		    <div id="sidebar-wrapper">
		        <nav id="spy">
		            <ul class="sidebar-nav nav">
		                <li class="sidebar-brand ${activeMenu == 'media' ? 'active' : ''}" >
		                    <a href="/libary">
		                    <div class="pic"><i class="glyphicon glyphicon-picture"></i></div>
		                    <div class="film"><i class="glyphicon glyphicon-film"></i></div>
		                     <span >Media</span>
		                    </a>
		                    <ul class="sl-submenu">
		                     <li><a href="/libary">Library</a></li>
		                     <li><a href="/newmedia">Add New</a></li>
		                    </ul>
		                </li>
		                <li class="${activeMenu == 'schedule' ? 'active' : ''}">
		                    <a href="/schedule">
		                    	<i class="glyphicon glyphicon-calendar"></i>
		                        <span class="nav-text">Schedule</span>
		                    </a>
		                </li>
		                <li class="${activeMenu == 'user' ? 'active' : ''}">
		                    <a href="/user">
		                    	<i class="glyphicon glyphicon-user"></i>
		                        <span class="nav-text">User</span>
		                    </a>
		                </li>
		                <li class="${activeMenu == 'report' ? 'active' : ''}">
		                    <a href="/report">
		                    	<i class="glyphicon glyphicon-list-alt"></i>
		                        <span class="nav-text">Report</span>
		                    </a>
		                </li>		                
		            </ul>
		            <footer id="footer">
		            	<div class="planb-footer-logo" ><img src="/images/planb-logo.png"/></div>
		            	<span class="version">ON WIFI CRM Version <g:meta name="app.version" /></span>
		            </footer>
		        </nav>
		    </div><!-- Sidebar -->
		    
		    <g:layoutBody/>
		    
		    <div class="modal custom fade" id="modal-loading" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
		      <div id="dialog-loading" class="modal-dialog">
		        <div class="modal-content">
		          <div id="loading-state"><img src="/images/loading.gif"></div>
		          <div id="loading-error"></div>
		          <div id="loading-success"></div>
		        </div>
		      </div>
		    </div>	
	            		
		</div><!--end wrapper-->
		<asset:javascript src="application.js"/>		
	</body>
</html>
