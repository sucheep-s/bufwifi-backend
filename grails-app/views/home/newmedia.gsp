<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="layout" content="main">
	<title></title>
</head>
<body>

    <div id="content-wrapper">

        <div class="content-header">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-header">
                        Add Media
                    </div>
                    <div class="btn-group" id="header-btn">
                        <input type="button" onclick="window.location='/libary'" class="btn btn-primary btn-sm" value="Back to libary"/>
                    </div>
                </div>
            </div>
        </div><!--content-header-->

        <div class="content-alert">
 
            <div id="alert-error" class="alert alert-danger" role="alert">
              <strong>Error!</strong> <span class="text-error">Invalid input.</span>
            </div>

            <div id="alert-success" class="alert alert-success" role="alert">
              <strong>Success!</strong> Please wait, Redirecting to media libary...
            </div>
        </div><!--content-inset-->

        <div class="content-main">

            <div class="gen-item">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="form-num" class="form-inline">
                            <span class="btn btn-mod btn-sm">Create item</span>
                            <div class="form-group form-group-num">
                                <input type="text" name="mediaNum" class="form-control input-sm" id="media-num" autofocus="autofocus" placeholder="0" maxlength="2" autocomplete="off"/>                           
                            </div>
                            <div class="form-group">
                                <span class="btn btn-mod btn-sm">item(s)</span>
                                <button type="submit" id="btn-gen" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-play"></i></button>
                                <div class="add-media-warning">*Limit 20 items per time.</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="form-field">

                <div class="media-field hide" id="fieldSet">
                    <form class="media-form">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group form-group-name">
                                    <input id="mediaName" name="mediaName" type="text" class="form-control mediaName" placeholder="Name"/>
                                </div>    
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group form-group-url">
                                    <input id="redirectUrl" name="redirectUrl" type="text" class="form-control redirectUrl" placeholder="Redirect Url"/>                          
                                </div>   
                            </div>                            
                            <div class="col-lg-4">
                                <div class="form-group form-group-path">
                                    <div class="inner-addon right-addon">
                                        <span class="path btn btn-path">Path</span> 
                                        <input id="pathFile" name="filePath" type="text" class="form-control pathFile" placeholder="Path File"/>                          
                                    </div>
                                </div>   
                            </div>   
                             <!--<div class="col-lg-2" >
                                <div class="inner-addon right-addon">
                                    <span class="path"><button type="button" class="btn btn-primary"><i id="dropdown" class="glyphicon glyphicon-triangle-bottom"></i></button></span> 
                                    <select class="form-control fileType" id="fileType" name="fileType">
                                      <option value="1">Video</option>
                                      <option value="2">Image</option>
                                    </select>
                                </div>
                            </div> -->                                   
                        </div>
                        <div class="row" id="text-area">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea id="desc" name="desc" class="form-control desc" rows="4" placeholder="Description"></textarea>
                                </div>    
                            </div>
                        </div> 
                        <hr/>
                    </form>
                </div>

            </div><!--end form field--> 
               
        <div class="add-media-btn">
          <button type="button" class="btn btn-danger" onclick="window.location='/libary'">Cancel</button>
          &nbsp;
          <button type="button" class="btn btn-success" onclick="addMedia()">Save</button>
        </div>

        </div><!--media field-->

    </div>
    <!-- Page Content -->	
</div>

</body>
</html>