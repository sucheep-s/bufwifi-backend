<!DOCTYPE html>
<html>
<head>
	<link rel='shortcut icon' type='image/x-icon' href='/images/favicon.ico' />
	<title>ON WIFI</title>
	<asset:stylesheet src="bootstrap.css"/>
	<asset:stylesheet src="login.css"/>
</head>
<body id="login-bg">

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div id="login-panel">
				<div class="panel-heading">
					<section class="left-heading">
						<h4>Login</h4>
						<div class="sub-heading">Administration System</div>
					</section>
					<section class="right-heading">
						<img src="/images/ONwifi.png" width="47px" height="33px" />
					</section>
				</div>
				<div class="body-panel">
					<div id="loading-login"><img src="/images/loading.gif"></div>
					<form id="form-login">
						<div class="form-group">
							<div class="inner-addon left-addon">
							    <i class="glyphicon glyphicon-user"></i>
							    <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" required />
							</div>

						</div>
						<div class="form-group">
							<div class="inner-addon left-addon">
							    <i class="glyphicon glyphicon-lock"></i>
							    <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required />
							</div>
							<div id="login-error" class="error">Wrong username or password.</div>
						</div>
						<div class="form-group">
							<input type="submit" id="login-submit" tabindex="3" class="form-control btn btn-info" value="Log-in">
						</div>						
					</form>
				</div>
				<div class="footer-panel">
					<img src="/images/planb-login.png">
				</div>			
			</div>
		</div>
	</div>
</div>
<asset:javascript src="jquery.min.js"/>
<asset:javascript src="bootstrap.js"/>
<asset:javascript src="jquery.validate.min.js"/>
<asset:javascript src="login.js"/>
</body>
</html>