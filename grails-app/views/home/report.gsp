<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="layout" content="main">
	<title></title>
</head>
<body>

    <div id="content-wrapper" class="content-report">

        <div class="content-header">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-header">
                        Report
                    </div>
                </div>
            </div>
        </div><!--content-header-->

        <div class="chart-report">
            <div class="row">
                <div class="col-lg-12">
                    <span class="header-summary">SUMMARY</span>
                </div>
            </div>
                <div class="body-report"> 
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="pie-report">
                                <div class="doughnut">
                                    <canvas id="mediaLogChart" width="250" height="250"></canvas>
                                    <div class="donut-inner">
                                        <h5 class="total-view"></h5>
                                        <span>VIEWS</span>
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                        <div class="col-lg-9" id="item-report">
                            <div class="detail-report">
                                <div class="date-report"> <span class="reportStart"></span> <span class="double-underline">to</span> <span class="reportEnd"></span> </div>                                   
                            </div>

                            <div class="section-item-report-detail">                                 

                            </div>
                   

                        </div>
                    </div>  
                </div>
        </div> 


        <div class="content-main">       

            <div class="gen-item">
                <div class="row">
                    <div class="col-lg-1">
                        <button class="btn btn-default btn-block btn-sm btn-filters" data-toggle="modal" data-target="#modal-filter">Filters</button>
                    </div>
                    <div class="col-lg-offset-8 col-lg-3">
                        
                        <div class="maxSelect">
                            <select id="maxSelect" class="form-control input-sm" onchange="reportCriteria()">
                              <option value="10" ${max==10?'selected':''} >10 per page</option>
                              <option value="20" ${max==20?'selected':''}>20 per page</option>
                              <option value="30" ${max==30?'selected':''}>30 per page</option>                                                
                            </select>                                
                        </div>

                        <div class="export-excel"><a data-toggle="tooltip" title="Export to excel" onclick="exportMediaLog(0)"><img src="/images/excel.png" width="22" height="22"></a></div>
                   
                    </div>
                </div>
            </div>
            <g:set var="no" value="${((max*page)-max)+1}"/>
            <div class="media-field">
                <section id="libary-tb">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="table-report" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="tr-report-num" width="7%">No</th>
                                        <th class="tr-report-media" width="19%">
                                            Media name
                                            <a onclick="reportSort('name', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="reportSort('name', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>              
                                        </th>
                                        <th class="tr-report-mobile" width="15%">
                                            Mobile number       
                                            <a onclick="reportSort('mobileNo', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="reportSort('mobileNo', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                               
                                        </th>    
                                        <th class="tr-report-request" width="15%">
                                            Request time 
                                            <a onclick="reportSort('requestTime', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="reportSort('requestTime', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                       
                                        </th>                                                                            
                                        <th class="tr-report-start" width="15%">
                                            Start time
                                            <a onclick="reportSort('startTime', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="reportSort('startTime', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                                        
                                        </th>
                                        <th class="tr-report-end" width="15%">
                                            End time 
                                            <a onclick="reportSort('endTime', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="reportSort('endTime', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                       
                                        </th>
                                        <th class="tr-report-time" width="14%">
                                            Play time (sec)
                                            <a onclick="reportSort('totalPlay', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="reportSort('totalPlay', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>
                                        </th>
                                    </tr>                               
                                </thead>
                                <tbody class="tb-user-body">
                                    <g:each var="media" in="${medias}" status="i">
                                        <tr>
                                            <td class="tr-report-num"><span class="span-num">${no++}</span></td>
                                            <td class="tr-report-media">${media.mediaName}</td>
                                            <td class="tr-report-mobile">${media.mobileNo ? media.mobileNo : ''}</td>
                                            <td class="tr-report-request">${media.requestTime}</td>
                                            <td class="tr-report-start">${media.startDate}</td>
                                            <td class="tr-report-end">${media.endDate}</td>
                                            <td class="tr-report-time">${media.totalPlay}</td>
                                        </tr>
  
                                    </g:each>                                                                                                                
                                </tbody>                                                                            
                            </table>
                        </div>
                    </div>
                </section>
                <section id="footer-report">
                    <div class="row">
                        <div class="col-lg-9">
                            <span class="btn btn-mod btn-page">Page ${page} of ${Math.ceil(total/max).toInteger()}</span>
                            <div id="report-selection"></div>
                        </div>
                        <div class="col-lg-3">
                            <div class="total-report">
                                Total Media : <span class="report-amount"><g:formatNumber number="${total}" type="number" /></span> view(s)
                            </div>
                        </div>                        
                    </div>                  
                </section>
            </div> 
            <input type="hidden" id="hiddenPage" value="${page}"/>
            <input type="hidden" id="hiddenTotal" value="${total}"/>
            <input type="hidden" id="hiddenMax" value="${max}"/>
            <input type="hidden" id="hiddenOrder" value="${order}"/>
            <input type="hidden" id="hiddenSort" value="${sort}"/>
            <input type="hidden" id="hiddenStartTime" value="${startTime}"/>
            <input type="hidden" id="hiddenEndTime" value="${endTime}"/>
            <input type="hidden" id="hiddenRequestTime" value="${requestTime}"/>
            <input type="hidden" id="hiddenUnsuccess" value="${unsuccess}"/>

    </div><!--end content-main-->
</div>


        <!--start filter modal-->
        <div class="modal fade" id="modal-filter" tabindex="-1" role="dialog" data-backdrop="static">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="filter-media-form">
                    
                    <div class="modal-report-header">
                        <div class="row">
                            <div class="col-lg-2">
                                Filters
                            </div>
                            <div class="col-lg-offset-9 col-lg-1">
                                <a class="close-filter" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-report-body">

                <div class="row"  id="filterStart">
                    <div class="col-lg-6">
                        <div class="date-input">
                            <div class="form-group">
                                <div class="inner-addon right-addon">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                    <input type="text" id="filterDateStart" name="filterDateStart" class="form-control input-sm filterDateStart" placeholder="Start Date*" tabindex="1" />
                                </div>       
                            </div>                         
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="time-section">
                            <div class="text-time">
                                Start Time*
                            </div>
                                <select class="form-control input-sm txtTime" id="filterHourStart" name="filterHourStart" tabindex="4"/>
                                    <option value="00" selected>00</option>
                                    <option value="01" >01</option>
                                    <option value="02" >02</option>
                                    <option value="03" >03</option>
                                    <option value="04" >04</option>
                                    <option value="05" >05</option>
                                    <option value="06" >06</option>
                                    <option value="07" >07</option>
                                    <option value="08" >08</option>
                                    <option value="09" >09</option>
                                    <option value="10" >10</option>
                                    <option value="11" >11</option>
                                    <option value="12" >12</option>
                                    <option value="13" >13</option>
                                    <option value="14" >14</option>
                                    <option value="15" >15</option>
                                    <option value="16" >16</option>
                                    <option value="17" >17</option>
                                    <option value="18" >18</option>
                                    <option value="19" >19</option>
                                    <option value="20" >20</option>
                                    <option value="21" >21</option>
                                    <option value="22" >22</option>
                                    <option value="23" >23</option>                                    
                                </select>   
                                <select class="form-control input-sm txtTime"  id="filterMinStart" name="filterMinStart" tabindex="3">
                                    <option value="00" selected>00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="59">59</option>
                                </select>                                   
                                <select class="form-control input-sm txtTime" id="filterSecStart" name="filterSecStart" tabindex="2">
                                    <option value="00" selected>00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="59">59</option>
                                </select>                                                                                      

                        </div>
                    </div>
                </div>

                <div class="row" id="filterEnd">
                    <div class="col-lg-6">
                        <div class="date-input">
                            <div class="form-group">
                                <div class="inner-addon right-addon">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                    <input type="text" id="filterDateEnd" name="filterDateEnd" class="form-control input-sm filterDateEnd" placeholder="End Date*" tabindex="4"/>
                                </div>     
                            </div>                           
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="time-section">

                            <div class="text-time">
                                End Time*
                            </div>  

                            <select class="form-control input-sm txtTime" id="filterHourEnd" name="filterHourEnd" tabindex="5"/>
                                <option value="00" selected>00</option>
                                <option value="01" >01</option>
                                <option value="02" >02</option>
                                <option value="03" >03</option>
                                <option value="04" >04</option>
                                <option value="05" >05</option>
                                <option value="06" >06</option>
                                <option value="07" >07</option>
                                <option value="08" >08</option>
                                <option value="09" >09</option>
                                <option value="10" >10</option>
                                <option value="11" >11</option>
                                <option value="12" >12</option>
                                <option value="13" >13</option>
                                <option value="14" >14</option>
                                <option value="15" >15</option>
                                <option value="16" >16</option>
                                <option value="17" >17</option>
                                <option value="18" >18</option>
                                <option value="19" >19</option>
                                <option value="20" >20</option>
                                <option value="21" >21</option>
                                <option value="22" >22</option>
                                <option value="23" >23</option>                                    
                            </select>   

                            <select class="form-control input-sm txtTime" id="filterMinEnd" name="filterMinEnd" tabindex="6">
                                <option value="00" selected>00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                                <option value="59">59</option>
                            </select>

                            <select class="form-control input-sm txtTime"  id="filterSecEnd" name="filterSecEnd" tabindex="7">
                                <option value="00" selected>00</option>
                                <option value="15">15</option>
                                <option value="30">30</option>
                                <option value="45">45</option>
                                <option value="59">59</option>
                            </select>  
                                                                                
                        </div>
                    </div>
                </div>  

                <div class="row">
                    <div class="col-lg-8">
                        <div class="unsuccessful">
                            <input type="checkbox" class="chk-success" name="checkSuccess" id="success" value="0" checked="" /> <span>Success</span>
                            <input type="checkbox" class="chk-success" name="checkSuccess" id="unsuccess" value="1" checked="" /> <span>Unsuccess</span>
                        </div>    
                    </div>
                    <div class="col-lg-4">
                        <div class="btn-footer-filter">
                            <button type="button" class="btn btn-reset btn-sm" onclick="resetReportField()">Reset</button>
                            <button type="submit" class="btn btn-primary btn-sm" onclick="filterReport()">Submit</button>                                
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="checkSuccess-error hide">*Please check at least one choice.</div>
                    </div>
                </div>                  
            </div>

                </form>

            </div><!--End modal content-->
          </div>
        </div><!--end modal-->  

</body>
</html>