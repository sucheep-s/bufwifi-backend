<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="layout" content="main">
	<title></title>
</head>
<body>

    <div id="content-wrapper" class="content-user">

        <div class="content-header">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-header">
                        User List
                    </div>
                </div>
            </div>
        </div><!--content-header-->

        <div class="content-main">

            <div class="gen-item">
                <div class="row">
                    <div class="col-lg-1">
                        <button class="btn btn-default btn-block btn-sm btn-filters" data-toggle="modal" data-target="#modal-user-filter">Filters</button>
                    </div>
                    <div class="col-lg-3">
                        <form id="form-search-user">
                            <div class="form-group input-user-search">
                                <input type="text" name="search" id="input-search" class="form-control input-sm" placeholder="10 Digits Mobile Number" maxlength="10" autocomplete="off"/>
                            </div>
                            <div class="btn-user-search">
                                <button type="submit" class="btn btn-sm"><i class="glyphicon glyphicon-search"></i></button>
                            </div>                            
                        </form>
                    </div>
                    <div class="col-lg-offset-5 col-lg-3">

                        <div class="maxSelect">
                            <select id="maxSelect" class="form-control input-sm" onchange="userCriteria()">
                              <option value="10" ${max==10?'selected':''} >10 per page</option>
                              <option value="20" ${max==20?'selected':''}>20 per page</option>
                              <option value="30" ${max==30?'selected':''}>30 per page</option>                                               
                            </select> 
                        </div>    
                        <div class="export-excel">
                            <a data-toggle="tooltip" title="Export to excel" onclick="exportUser(0)"><img src="/images/excel.png" width="22" height="22"></a>
                        </div>         

                    </div>
                </div>
            </div>
            <g:set var="no" value="${((max*page)-max)+1}"/>
            <div class="media-field">
                <section id="libary-tb">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="table-user" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="tr-num">No</th>
                                        <th class="tr-phone">
                                            Mobile Number 
                                            <a onclick="userSort('mobileNo', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="userSort('mobileNo', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>
                                        </th>
                                        <th class="tr-birthday">
                                            Date of Birth
                                            <a onclick="userSort('dateOfBirth', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="userSort('dateOfBirth', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                           
                                        </th>
                                        <th class="tr-gender">
                                            Gender
                                            <a onclick="userSort('gender', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="userSort('gender', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                            
                                        </th>
                                        <th class="tr-created">
                                            Created Dated
                                            <a onclick="userSort('createDate', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="userSort('createDate', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                            
                                        </th>
                                    </tr>                               
                                </thead>
                                <tbody class="tb-user-body">
                                    <g:each var="user" in="${users}" status="i">
                                        <tr>
                                            <td class="tr-num"><span class="span-num">${no++}</span></td>
                                            <td class="tr-phone">${user.mobileNo}</td>
                                            <td class="tr-birthday">${user.dateOfBirth}</td>
                                            <td class="tr-gender">${user.gender==1?'Male':'Female'}</td>
                                            <td class="tr-created">${user.createDate}</td>
                                        </tr>  
                                    </g:each>                            
                                </tbody>                                                                            
                            </table>
                        </div>
                    </div>
                </section>
                <section id="footer-user">
                    <div class="row">
                        <div class="col-lg-9">
                            <span class="btn btn-mod btn-page">Page ${page} of ${Math.ceil(total/max).toInteger()}</span>
                            <div id="user-selection"></div>
                        </div>
                        <div class="col-lg-3">
                            <div class="total-user">
                                Total User : <span class="user-amount"><g:formatNumber number="${total}" type="number" /></span> users
                            </div>
                        </div>                        
                    </div>                  
                </section>

            </div><!--media field-->
            <input type="hidden" id="hiddenPage" value="${page}"/>
            <input type="hidden" id="hiddenTotal" value="${total}"/>
            <input type="hidden" id="hiddenMax" value="${max}"/>
            <input type="hidden" id="hiddenOrder" value="${order}"/>
            <input type="hidden" id="hiddenSort" value="${sort}"/>
            <input type="hidden" id="hiddenStartTime" value="${startTime}"/>
            <input type="hidden" id="hiddenEndTime" value="${endTime}"/>
    </div>
    <!-- Page Content -->


        <!--start filter modal-->
        <div class="modal fade" id="modal-user-filter" tabindex="-1" role="dialog" data-backdrop="static">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="filter-user-form">
                    
                    <div class="modal-report-header">
                        <div class="row">
                            <div class="col-lg-2">
                                Filters
                            </div>
                            <div class="col-lg-offset-9 col-lg-1">
                                <a class="close-filter" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-report-body">

                <div class="row"  id="filterStart">
                    <div class="col-lg-6">
                        <div class="date-input">
                            <div class="form-group">
                                <div class="inner-addon right-addon">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                    <input type="text" id="userExportDateStart" name="userExportDateStart" class="form-control input-sm userExportDateStart" placeholder="Start Date*" tabindex="1"/>
                                </div>       
                            </div>                         
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="time-section">
                            <div class="text-time">
                                Start Time*
                            </div>
                                <select class="form-control input-sm txtTime" id="userExportHourStart" name="userExportHourStart" tabindex="4"/>
                                    <option value="00" selected>00</option>
                                    <option value="01" >01</option>
                                    <option value="02" >02</option>
                                    <option value="03" >03</option>
                                    <option value="04" >04</option>
                                    <option value="05" >05</option>
                                    <option value="06" >06</option>
                                    <option value="07" >07</option>
                                    <option value="08" >08</option>
                                    <option value="09" >09</option>
                                    <option value="10" >10</option>
                                    <option value="11" >11</option>
                                    <option value="12" >12</option>
                                    <option value="13" >13</option>
                                    <option value="14" >14</option>
                                    <option value="15" >15</option>
                                    <option value="16" >16</option>
                                    <option value="17" >17</option>
                                    <option value="18" >18</option>
                                    <option value="19" >19</option>
                                    <option value="20" >20</option>
                                    <option value="21" >21</option>
                                    <option value="22" >22</option>
                                    <option value="23" >23</option>                                    
                                </select>   
                                <select class="form-control input-sm txtTime"  id="userExportMinStart" name="userExportMinStart" tabindex="3">
                                    <option value="00">00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="59">59</option>
                                </select>                                   
                                <select class="form-control input-sm txtTime" id="userExportSecStart" name="userExportSecStart" tabindex="2">
                                    <option value="00">00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="59">59</option>
                                </select>                                                               
                        </div>
                    </div>
                </div>

                <div class="row" id="filterEnd">
                    <div class="col-lg-6">
                        <div class="date-input">
                            <div class="form-group">
                                <div class="inner-addon right-addon">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                    <input type="text" id="userExportDateEnd" name="userExportDateEnd" class="form-control input-sm userExportDateEnd" placeholder="End Date*" tabindex="5"/>
                                </div>     
                            </div>                           
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="time-section">
   
                            <div class="text-time">
                                End Time*
                            </div>   

                                <select class="form-control input-sm txtTime" id="userExportHourEnd" name="userExportHourEnd" tabindex="4"/>
                                    <option value="00" selected>00</option>
                                    <option value="01" >01</option>
                                    <option value="02" >02</option>
                                    <option value="03" >03</option>
                                    <option value="04" >04</option>
                                    <option value="05" >05</option>
                                    <option value="06" >06</option>
                                    <option value="07" >07</option>
                                    <option value="08" >08</option>
                                    <option value="09" >09</option>
                                    <option value="10" >10</option>
                                    <option value="11" >11</option>
                                    <option value="12" >12</option>
                                    <option value="13" >13</option>
                                    <option value="14" >14</option>
                                    <option value="15" >15</option>
                                    <option value="16" >16</option>
                                    <option value="17" >17</option>
                                    <option value="18" >18</option>
                                    <option value="19" >19</option>
                                    <option value="20" >20</option>
                                    <option value="21" >21</option>
                                    <option value="22" >22</option>
                                    <option value="23" >23</option>                                    
                                </select>   
                                <select class="form-control input-sm txtTime"  id="userExportMinEnd" name="userExportMinEnd" tabindex="3">
                                    <option value="00" selected>00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="59">59</option>
                                </select>                                   
                                <select class="form-control input-sm txtTime" id="userExportSecEnd" name="userExportSecEnd" tabindex="2">
                                    <option value="00" >00</option>
                                    <option value="15">15</option>
                                    <option value="30">30</option>
                                    <option value="45">45</option>
                                    <option value="59">59</option>
                                </select>   
                        </div>
                    </div>
                </div>  

                <div class="row">
                    <div class="col-lg-offset-8 col-lg-4">
                        <div class="btn-footer-filter">
                            <button type="button" class="btn btn-reset btn-sm" onclick="resetUserExportField()">Reset</button>
                            <button type="submit" class="btn btn-primary btn-sm" onclick="filterUser()">Submit</button>                                
                        </div>
                    </div>
                </div>                  
            </div>

                </form>

            </div><!--End modal content-->
          </div>
        </div><!--end modal-->


</div>

</body>
</html>