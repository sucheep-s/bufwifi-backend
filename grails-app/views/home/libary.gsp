<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="layout" content="main">
	<title></title>
</head>
<body>

    <div id="content-wrapper" class="content-libary">

        <div class="content-header">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-header">
                        Media Libary
                    </div>
                    <div class="btn-group" id="header-btn">
                        <input type="button" onclick="window.location='/newmedia'" class="btn btn-primary btn-sm" value="Add New"/>
                    </div>
                </div>
            </div>
        </div><!--content-header-->

        <div class="content-alert">
 			
 			<g:if test="${flash.error}">
	            <div id="alert-error" class="alert alert-danger" role="alert">
	              <strong>Error!</strong> ${flash.error}
	            </div> 				
 			</g:if>

            <div id="alert-error" class="alert alert-danger" role="alert">
              <strong>Error!</strong> Invalid input.
            </div>

            <div id="alert-success" class="alert alert-success" role="alert">
              <strong>Success!</strong> Please wait, Redirecting to media libary...
            </div>
        </div><!--content-inset-->

        <div class="content-main">

            <div class="gen-item">
                <div class="row">
                    <div class="col-lg-1 col-lg-offset-9">
                    	<button id="del-libary" class="btn btn-danger btn-sm btn-block" onclick="modalDeleteMedia()">Delete</button>
                    </div>
                    <div class="col-lg-2">
                        <select id="maxSelect" class="form-control input-sm" onchange="mediaCriteria()">
                          <option value="10" ${max==10?'selected':''} >10 per page</option>
                          <option value="20" ${max==20?'selected':''}>20 per page</option>
                          <option value="30" ${max==30?'selected':''}>30 per page</option>                                               
                        </select>                    	
                    </div>
                </div>
            </div>

            <div class="media-field">
            	<section id="libary-tb">
	            	<div class="row">
	            		<div class="col-lg-12">
	            			<table id="table-libary" class="table table-striped">
	            				<thead>
		            				<tr>
		            					<th class="tr-chk"><input id="chk" type="checkbox" onclick="checkMediaAll(this)"/></th>
		            					<th class="tr-name">
		            						Name
                                            <a onclick="mediaSort('name', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="mediaSort('name', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>
		            					</th>
		            					<th class="tr-desc">Description</th>
		            					<th class="tr-createdDate">
		            						Created Date
                                            <a onclick="mediaSort('createDate', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="mediaSort('createDate', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>		            						
		            					</th>
		            					<th class="tr-redirect">Redirect Link</th>
		            					<th class="tr-path">Path File</th>
		            					<th class="tr-edit"></th>
		            				</tr>            					
	            				</thead>

	            				<tbody>
	            					<g:each var="media" in="${medias}">
			            				<tr id="${media.id}" class="row-media">
			            					<td class="tr-chk"><input type="checkbox" class="chk" value="${media.id}"/></td>
			            					<td id="name1" class="tr-name">
				            					<span id="spanName${media.id}">${media.name}</span>
				            						<input class="hide inputMediaName" type="text" id="txtName${media.id}" value="${media.name}"/>
				            					<input type="hidden" id="hiddenName${media.id}" value="${media.name}"/>
			            					</td>
			            					<td id="desc1" class="tr-desc">
				            					<span id="spanDesc${media.id}">${media.desc}</span>
				            					<input class="hide" type="text" id="txtDesc${media.id}" value="${media.desc}"/>
				            					<input type="hidden" id="hiddenDesc${media.id}" value="${media.desc}"/>
			            					</td>
			            					<td class="tr-createdDate">
				            					${media.createDate}
			            					</td>
			            					<td id="url${media.u}" class="tr-redirect">
			            						<span id="spanUrl${media.id}">${media.url}</span>
			            						<input class="hide inputMediaUrl" type="text" id="txtUrl${media.id}" value="${media.url}"/>
			            						<input type="hidden" id="hiddenUrl${media.id}" value="${media.url}"/>
			            					</td>			            					
			            					<td id="path${media.id}" class="tr-path">
			            						<span id="spanPath${media.id}">${media.path}</span>
			            						<input class="hide inputMediaPath" type="text" id="txtPath${media.id}" value="${media.path}"/>
			            						<input type="hidden" id="hiddenPath${media.id}" value="${media.path}"/>
			            					</td>
			            					<td class="tr-edit">
			            						<a data-id="${media.id}" id="cancel${media.id}" class="cancel hide" data-toggle="tooltip" title="Cancel" onclick="cancelActive(this)">
			            							<i class="glyphicon glyphicon-ban-circle"></i>
			            						</a>
			            						<a id="save${media.id}" onclick="updateMedia(${media.id})" class="save hide" data-toggle="tooltip" title="Save">
			            							<i class="glyphicon glyphicon-ok"></i>
			            						</a>
			            						<a id="edit${media.id}" data-id="${media.id}" onclick="activeLibary(this)" class="edit" data-toggle="tooltip" title="Edit">
			            							<i class="glyphicon glyphicon-edit"></i>
			            						</a>
			            					</td>
			            				</tr>
	            					</g:each>	            				           					
	            				</tbody>          				         				            				
	            			</table>
	            		</div>
	            	</div>
            	</section>
                <section id="footer-libary">
                    <div class="row">
                        <div class="col-lg-9">
                        	<span class="btn btn-mod btn-page">Page ${page} of ${Math.ceil(total/max).toInteger()}</span>
                            <div id="media-selection"></div>
                        </div>
                        <div class="col-lg-3">
                            <div class="total-user">
                                Total Media : <span class="user-amount"><g:formatNumber number="${total}" type="number" /></span> media(s)
                            </div>
                        </div>                        
                    </div>                  
                </section>

            </div><!--media field-->

	    </div>
	    <!-- Page Content -->

	    <!--          MODAL          -->
	    <div class="modal fade" id="del-media-modal" tabindex="-1" role="dialog" data-backdrop="static">
	      <div id="modal-dialog-del-media" class="modal-dialog" role="document">
	        <div class="modal-content" id="modal-content-del-media">
	        <div class="row">
	          <div class="col-lg-12">
	            <h3>Delete Confirmation</h3>
	          </div>
	        </div>
	        <div class="row">
	          <div class="col-lg-12">
	            <br/>
	            This action will delete the <span class="modal-underline">media(s)</span>.
	            <br/>
	            Are you sure to delete it?
	          </div>
	        </div>
	        <div id="btn-del-zone" class="row">
	          <div class="col-lg-6">
	            <button type="button" class="btn btn-gray btn-block" data-dismiss="modal">Cancel</button>
	          </div>
	          <div class="col-lg-6">
	            <button type="button" onclick="deleteMedia()" class="btn btn-danger btn-block">Delete</button>
	          </div>
	        </div>
	        </div>
	      </div>
	    </div><!--end delete modal-->    
	    <!--          MODAL          -->     
        <input type="hidden" id="hiddenPage" value="${page}"/>
        <input type="hidden" id="hiddenTotal" value="${total}"/>
        <input type="hidden" id="hiddenMax" value="${max}"/> 
        <input type="hidden" id="hiddenOrder" value="${order}"/>
        <input type="hidden" id="hiddenSort" value="${sort}"/>        	
	</div>

</body>
</html>