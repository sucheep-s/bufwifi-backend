<div class="gen-item">
    <div class="row">
        <div class="col-lg-1">
            <button class="btn btn-default btn-block btn-sm btn-filters" data-toggle="modal" data-target="#modal-filter">Filters</button>
        </div>
        <div class="col-lg-offset-8 col-lg-3">
            
            <div class="maxSelect">
                <select id="maxSelect" class="form-control input-sm" onchange="reportCriteria()">
                  <option value="10" ${max==10?'selected':''} >10 per page</option>
                  <option value="20" ${max==20?'selected':''}>20 per page</option>
                  <option value="30" ${max==30?'selected':''}>30 per page</option>                                                
                </select>                                
            </div>

            <div class="export-excel"><a data-toggle="tooltip" title="Export to excel" onclick="exportMediaLog(1)"><img src="/images/excel.png" width="22" height="22"></a></div>
       
        </div>
    </div>
</div>
<g:set var="no" value="${((max*page)-max)+1}"/>
<div class="media-field">
    <section id="libary-tb">
        <div class="row">
            <div class="col-lg-12">
                <table id="table-report" class="table table-striped">
                    <thead>
                        <tr>
                            <th class="tr-report-num" width="7%">No</th>
                            <th class="tr-report-media" width="19%">
                                Media name
                                <a onclick="reportSort('name', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                <a onclick="reportSort('name', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>              
                            </th>
                            <th class="tr-report-mobile" width="15%">
                                Mobile number       
                                <a onclick="reportSort('mobileNo', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                <a onclick="reportSort('mobileNo', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                               
                            </th>    
                            <th class="tr-report-request" width="15%">
                                Request time 
                                <a onclick="reportSort('requestTime', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                <a onclick="reportSort('requestTime', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                       
                            </th>                                                                            
                            <th class="tr-report-start" width="15%">
                                Start time
                                <a onclick="reportSort('startTime', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                <a onclick="reportSort('startTime', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                                        
                            </th>
                            <th class="tr-report-end" width="15%">
                                End time 
                                <a onclick="reportSort('endTime', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                <a onclick="reportSort('endTime', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                       
                            </th>
                            <th class="tr-report-time" width="14%">
                                Play time (sec)
                                <a onclick="reportSort('totalPlay', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                <a onclick="reportSort('totalPlay', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>
                            </th>
                        </tr>                               
                    </thead>
                    <tbody class="tb-user-body">
                        <g:each var="media" in="${medias}" status="i">
                            <tr>
                                <td class="tr-report-num"><span class="span-num">${no++}</span></td>
                                <td class="tr-report-media">${media.mediaName}</td>
                                <td class="tr-report-mobile">${media.mobileNo ? media.mobileNo : ''}</td>
                                <td class="tr-report-request">${media.requestTime}</td>
                                <td class="tr-report-start">${media.startDate}</td>
                                <td class="tr-report-end">${media.endDate}</td>
                                <td class="tr-report-time">${media.totalPlay}</td>
                            </tr>

                        </g:each>                                                                                                                
                    </tbody>                                                                            
                </table>
            </div>
        </div>
    </section>
    <section id="footer-report">
        <div class="row">
            <div class="col-lg-9">
                <span class="btn btn-mod btn-page">Page ${page} of ${Math.ceil(total/max).toInteger()}</span>
                <div id="report-selection"></div>
            </div>
            <div class="col-lg-3">
                <div class="total-report">
                    Total Media : <span class="report-amount"><g:formatNumber number="${total}" type="number" /></span> view(s)
                </div>
            </div>                        
        </div>                  
    </section>
</div>

<input type="hidden" id="hiddenPage" value="${page}"/>
<input type="hidden" id="hiddenTotal" value="${total}"/>
<input type="hidden" id="hiddenMax" value="${max}"/>
<input type="hidden" id="hiddenOrder" value="${order}"/>
<input type="hidden" id="hiddenSort" value="${sort}"/>
<input type="hidden" id="hiddenStartTime" value="${startTime}"/>
<input type="hidden" id="hiddenEndTime" value="${endTime}"/>
<input type="hidden" id="hiddenRequestTime" value="${requestTime}"/>
<input type="hidden" id="hiddenUnsuccess" value="${unsuccess}"/>
