

<div class="gen-item">
    <div class="row">
        <div class="col-lg-2">
            <input type="button" onclick="window.location='/user'" class="btn btn-primary btn-sm" value="Back to User"/>
        </div>
    </div>
</div>

<div class="media-field">
    <section id="libary-tb">
        <div class="row">
            <div class="col-lg-12">
                <table id="table-user" class="table table-striped">
                    <thead>
                        <tr>
                            
                            <th>Mobile Number</th>
                            <th>Date of Birth</th>
                            <th>Gender</th>
                            <th>Created Dated</th>
                            <th>Reset Password</th>

                        </tr>                               
                    </thead>
                    <tbody class="tb-user-body">

                        <g:if test="${result == "No user found"}">
                            <td></td>
                            <td></td>
                            <td style="color:red;">${result}</td>
                            <td></td>
                            <td></td>
                        </g:if>
                        <g:else>
                            <tr>
                                <td>${user.mobileNo}</td>
                                <td>${user.dateOfBirth}</td>
                                <td>${user.gender==1?'Male':'Female'}</td>
                                <td>${user.createDate}</td>
                                <td>
                                    <a class="resetPassword" href="#"  data-toggle="modal" data-target="#modal-user-reset">Reset Password</a>
                                    <span class="smsSent hide"><i class="glyphicon glyphicon-ok"></i>SMS sent</span>
                                </td>

                            </tr>                             
                        </g:else>
                          
                    </tbody>                                                                            
                </table>
            </div>
        </div>
    </section>

</div><!--media field-->

<!--reset modal-->
<div class="modal fade" id="modal-user-reset" tabindex="-1" role="dialog" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-content-del-media">

        <div class="row">
          <div class="col-lg-12">
            <h3>Reset Password Confirmation</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <br/>
            This action will reset this <span class="modal-underline">user's password</span>.
            <br/>
            Are you sure to reset?
          </div>
        </div>
        <div id="btn-del-zone" class="row">
          <div class="col-lg-6">
            <button type="button" class="btn btn-gray btn-block" data-dismiss="modal">Cancel</button>
          </div>
          <div class="col-lg-6">
            <button type="button" class="btn btn-primary btn-block" onclick="resetUserPassword('${user.mobileNo}')">Confirm</button>
          </div>
        </div>

    </div><!--End modal content-->
  </div>
</div><!--end modal-->