

            <div class="gen-item">
                <div class="row">
                    <div class="col-lg-1">
                        <button class="btn btn-default btn-block btn-sm btn-filters" data-toggle="modal" data-target="#modal-user-filter">Filters</button>
                    </div>
                    <div class="col-lg-3">
                        <form id="form-search-user">
                            <div class="form-group input-user-search">
                                <input type="text" name="search" id="input-search" class="form-control input-sm" placeholder="10 Digits Mobile Number" maxlength="10" autocomplete="off"/>
                            </div>
                            <div class="btn-user-search">
                                <button type="submit" class="btn btn-sm"><i class="glyphicon glyphicon-search"></i></button>
                            </div>                            
                        </form>
                    </div>                   
                    <div class="col-lg-offset-5 col-lg-3">

                        <div class="maxSelect">
                            <select id="maxSelect" class="form-control input-sm" onchange="userCriteria()">
                              <option value="10" ${max==10?'selected':''} >10 per page</option>
                              <option value="20" ${max==20?'selected':''}>20 per page</option>
                              <option value="30" ${max==30?'selected':''}>30 per page</option>                                               
                            </select> 
                        </div>    
                        <div class="export-excel">
                            <a data-toggle="tooltip" title="Export to excel" onclick="exportUser(1)"><img src="/images/excel.png" width="22" height="22"></a>
                        </div>         

                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-lg-12">
                        <g:if test="${showStart}">
                            <div class="userFilterDate"><span class="reportStart"><g:formatDate format="yyyy-MM-dd H:mm:ss"  date="${showStart}" /></span> <span class="doubleunderline">to</span> <span class="reportEnd"><g:formatDate format="yyyy-MM-dd H:mm:ss"  date="${showEnd}" /></span></div>
                        </g:if>
                    </div>
                </div>            
            <g:set var="no" value="${((max*page)-max)+1}"/>
            <div class="media-field">
                <section id="libary-tb">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="table-user" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="tr-num">No</th>
                                        <th class="tr-phone">
                                            Mobile Number 
                                            <a onclick="userSort('mobileNo', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="userSort('mobileNo', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>
                                        </th>
                                        <th class="tr-birthday">
                                            Date of Birth
                                            <a onclick="userSort('dateOfBirth', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="userSort('dateOfBirth', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                           
                                        </th>
                                        <th class="tr-gender">
                                            Gender
                                            <a onclick="userSort('gender', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="userSort('gender', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                            
                                        </th>
                                        <th class="tr-created">
                                            Created Dated
                                            <a onclick="userSort('createDate', 'asc')" class="sort-table"><i class="glyphicon glyphicon-triangle-top"></i></a>
                                            <a onclick="userSort('createDate', 'desc')" class="sort-table"><i class="glyphicon glyphicon-triangle-bottom"></i></a>                                            
                                        </th>
                                    </tr>                               
                                </thead>
                                <tbody class="tb-user-body">
                                    <g:each var="user" in="${users}" status="i">
                                        <tr>
                                            <td class="tr-num"><span class="span-num">${no++}</span></td>
                                            <td class="tr-phone">${user.mobileNo}</td>
                                            <td class="tr-birthday">${user.dateOfBirth}</td>
                                            <td class="tr-gender">${user.gender==1?'Male':'Female'}</td>
                                            <td class="tr-created">${user.createDate}</td>
                                        </tr>  
                                    </g:each>                            
                                </tbody>                                                                            
                            </table>
                        </div>
                    </div>
                </section>
                <section id="footer-user">
                    <div class="row">
                        <div class="col-lg-9">
                            <span class="btn btn-mod btn-page">Page ${page} of ${Math.ceil(total/max).toInteger()}</span>
                            <div id="user-selection"></div>
                        </div>
                        <div class="col-lg-3">
                            <div class="total-user">
                                Total User : <span class="user-amount"><g:formatNumber number="${total}" type="number" /></span> users
                            </div>
                        </div>                        
                    </div>                  
                </section>

            </div><!--media field-->
            <input type="hidden" id="hiddenPage" value="${page}"/>
            <input type="hidden" id="hiddenTotal" value="${total}"/>
            <input type="hidden" id="hiddenMax" value="${max}"/>
            <input type="hidden" id="hiddenOrder" value="${order}"/>
            <input type="hidden" id="hiddenSort" value="${sort}"/>
            <input type="hidden" id="hiddenStartTime" value="${startTime}"/>
            <input type="hidden" id="hiddenEndTime" value="${endTime}"/>
    