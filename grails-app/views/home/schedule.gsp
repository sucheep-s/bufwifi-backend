<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="layout" content="main">
	<title></title>
</head>
<body>

    <div id="content-wrapper" class="content-schedule">

        <div class="content-header">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-header">
                        Schedule
                    </div>
                </div>
            </div>
        </div><!--content-header-->
        <div class="content-alert">

            <g:if test="${flash.error}">
                <div id="alert-error" class="alert alert-danger" role="alert">
                  <strong>Error!</strong> ${flash.error}
                </div>
            </g:if>

            <div id="alert-error" class="alert alert-danger" role="alert">
              <strong>Error!</strong> <span class="text-error">Invalid input.</span>
            </div>

            <div id="alert-success" class="alert alert-success" role="alert">
              <strong>Success!</strong> Please wait, Redirecting to media libary...
            </div>
        </div><!--content-inset-->
        <div class="content-main">
          <div class="row">
            <!-- Playlist -->
            <div class="col-lg-3">
              <div id="external-events" class="playlist">             
                <g:each var="playlist" in="${playlists}">
                  <div class="external-event" id="event${playlist.id}"><span data-q="1" class="btn btn-sm btn-block">${playlist.name}</span><input type="hidden"/ class="hiddenPlaylist" value="${playlist.id}" /></div>
                </g:each>
              </div>
                <div class="new-playlist">
                  <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modal-create-schedule">+ Create New Playlist</button>
                </div>               
            </div>
            <!-- Playlist -->

            <!-- Calendar -->
            <div class="col-lg-9">
              <div id="calendar"></div>
            </div>
            <!-- Calendar -->

          </div>

        </div>

        <!--                    Modal                    -->

        <!--Modal create playlist-->
        <div class="modal fade" id="modal-create-schedule" tabindex="-1" role="dialog" data-backdrop="static">
          <div id="modal-create-form" class="modal-dialog">
            <div class="modal-content">

                <section class="section-playlist">
                  <form id="form-create-playlist">              
                    <div class="playlist-header">
                      <div class="create-playlist-icon">
                          <div class="mix-icon-modal">
                            <a onclick="removeMediaInPlaylist()" data-toggle="tooltip" title="Remove checked media">
                              <div class="glyph-a"><i class="glyphicon glyphicon-picture"></i></div>
                              <div class="glyph-b"><i class="glyphicon glyphicon-film"></i></div>
                              <div class="glyph-c"><i class="glyphicon glyphicon-remove"></i></div>
                            </a>
                          </div>
                      </div><!--form-icon-->
                      <div class="form-input-playlist" id="input-create-playlistName">
                        <div class="form-group">
                          <input id="input-create-playlist" type="text" class="form-control input-xs input-playlist" name="input-create-playlist" placeholder="Playlist name" maxlength="45"  autofocus/>
                        </div>
                      </div>
                    </div><!--end form header-->

                    <hr/>

                    <div class="table-modal">
                      <table class="table table-striped table-playlist">
                        <tbody id="body-create-row">
                          <!-- PLaylist show here -->  
                        </tbody>
                      </table>
                    </div><!--end playlist table-->
                    <div class="playlist-input-footer">
                      <div class="desc-input" id="desc-input-create">
                        <div class="form-group">
                          <input id="input-create-desc" name="input-create-des" type="text" class="form-control input-sm" placeholder="Description" />                          
                        </div>
                      </div>                      
                      <div class="date-input">
                        <div class="form-group" id="form-create-startDate">
                          <div class="inner-addon right-addon date1">
                              <i class="glyphicon glyphicon-calendar"></i>
                              <input type="text" id="txtCreateStart" class="form-control input-sm txtStart" placeholder="Start Date"/>
                          </div>
                        </div>
                        <div class="form-group" id="form-create-endDate">                        
                          <div class="inner-addon right-addon date2">
                              <i class="glyphicon glyphicon-calendar"></i>
                              <input type="text" id="txtCreateEnd" class="form-control input-sm txtEnd" placeholder="End Date"/>
                          </div>
                        </div>
                      </div><!--end date-input-->
                      <div class="playlist-footer">
                        <div class="summary-playlist">
                          <span class="sum-media">0</span>&nbsp;media(s)
                        </div>
                        <div class="playlist-footer-btn">
                          <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                          <button type="submit" class="btn btn-success" >Save</button>
                        </div>
                      </div><!--end playlist footer-->
                    </div>
                  </form>    
                </section>

                <section class="section-media">

                  <div class="row">
                    <div id="media-header" class="col-lg-12">
                      <div class="media-header-text">
                        <h3>Select media to playlist</h3>
                      </div>
                      <div class="close-modal">
                        <a data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></a>
                      </div>
                    </div>
                  </div><!--end modal header-->
                  <div class="row">
                    <div id="media-search" class="col-lg-12">
                        <div class="inner-addon right-addon">
                            <i class="glyphicon glyphicon-search"></i>
                            <input type="text" id="txtCreateSearch" class="form-control" placeholder="Search"/>
                        </div>
                    </div>
                  </div><!--end search-->
                  <div class="row">
                    <div id="table-media-modal-zone" class="col-lg-12">
                      <table id="table-media-modal" class="table table-striped">
                        <tbody id="tbody-create-media">
                          <g:each var="media" in="${medias}">
                            <tr id="create-row${media.id}" class="row-media">
                              <td class="tr-media-type">
                                <g:if test="${media.idMediaType} == 1"><i class="glyphicon glyphicon-film"></i></g:if>
                                <g:else><i class="glyphicon glyphicon-picture"></i></g:else>
                              </td>
                              <td class="tr-media-name"><a data-toggle="tooltip" title="${media.url}">${media.name}</a></td>
                              <td class="tr-media-add"><button class="btn btn-link btn-xs" onclick="addToCreatePlaylist('${media.id}', '${media.name}', ${media.idMediaType}, '${media.url}')"><i class="glyphicon glyphicon-plus"></i></button></td>
                            </tr>
                          </g:each>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </section>

            </div><!--end content-->
          </div>
        </div><!--end modal-->        
        <!--Modal create playlist-->

        <!--Modal edit playlist-->
        <div class="modal fade" id="modal-edit-schedule" tabindex="-1" role="dialog" data-backdrop="static">
          <div id="modal-edit-form" class="modal-dialog">
            <div class="modal-content">
            <input type="hidden" id="hiddenPlaylistId" />
                <section class="section-playlist">
                  <form id="form-edit-playlist">              
                    <div class="playlist-header">
                      <div class="edit-playlist-icon">
                          <div class="mix-icon-modal">
                          <a onclick="removeMediaInPlaylist()" data-toggle="tooltip" title="Remove checked media">
                            <div class="glyph-a"><i class="glyphicon glyphicon-picture"></i></div>
                            <div class="glyph-b"><i class="glyphicon glyphicon-film"></i></div>
                            <div class="glyph-c"><i class="glyphicon glyphicon-remove"></i></div>
                          </a>
                          </div>
                          <div class="normal-icon-modal">
                            <a onclick="refreshMediaInPlaylist()" data-toggle="tooltip" title="Refresh playlist"><i class="glyphicon glyphicon-repeat"></i></a>
                          </div>
                      </div><!--form-icon-->
                      <div class="form-input-playlist" id="input-edit-playlistName">
                        <div class="form-group">
                          <input id="input-edit-playlist" type="text" class="form-control input-xs input-playlist" name="input-edit-playlist" placeholder="Playlist name"  maxlength="45"/>
                        </div>
                      </div>
                      <div class="button-playlist">
                        <button id="btn-del-playlist" type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#del-playlist-modal">Delete Playlist</button>
                      </div>                      
                    </div><!--end form header-->

                    <hr/>

                    <div class="table-modal">
                    <input type="hidden" id="hiddenPlaylistId" />
                      <table class="table table-striped table-playlist">
                        <tbody id="body-edit-row">
                          <!-- PLaylist show here -->                                            
                        </tbody>
                      </table>
                    </div><!--end playlist table-->
                    <div class="playlist-input-footer">
                      <div class="desc-input" id="desc-input-edit">
                        <div class="form-group">
                          <input id="input-edit-desc" name="input-edit-desc" type="text" class="form-control input-sm" placeholder="Description" />                          
                        </div>
                      </div>                      
                      <div class="date-input">
                        <div class="form-group" id="form-edit-startDate">
                          <div class="inner-addon right-addon date1">
                              <i class="glyphicon glyphicon-calendar"></i>
                              <input type="text" id="txtEditStart" class="form-control input-sm txtStart" placeholder="Start Date"/>
                          </div>
                        </div>
                        <div class="form-group" id="form-edit-endDate">                        
                          <div class="inner-addon right-addon date2">
                              <i class="glyphicon glyphicon-calendar"></i>
                              <input type="text" id="txtEditEnd" class="form-control input-sm txtEnd" placeholder="End Date"/>
                          </div>
                        </div>
                      </div><!--end date-input-->
                      <div class="playlist-footer">
                        <div class="summary-playlist">
                          <span class="sum-media">0</span>&nbsp;media(s)
                        </div>
                        <div class="playlist-footer-btn">
                          <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                          <button type="submit" class="btn btn-success" >Save</button>
                        </div>
                      </div><!--end playlist footer-->
                    </div>
                  </form>    
                </section>

                <section class="section-media">

                  <div class="row">
                    <div id="media-header" class="col-lg-12">
                      <div class="media-header-text">
                        <h3>Select media to playlist</h3>
                      </div>
                      <div class="close-modal">
                        <a data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i></a>
                      </div>
                    </div>
                  </div><!--end modal header-->
                  <div class="row">
                    <div id="media-search" class="col-lg-12">
                        <div class="inner-addon right-addon">
                            <i class="glyphicon glyphicon-search"></i>
                            <input type="text" class="form-control" id="txtEditSearch" placeholder="Search"/>
                        </div>
                    </div>
                  </div><!--end search-->
                  <div class="row">
                    <div id="table-media-modal-zone" class="col-lg-12">
                      <table id="table-media-modal" class="table table-striped">
                        <tbody id="tbody-edit-media">

                          <g:each var="media" in="${medias}">
                            <tr id="edit-row${media.id}" class="row-media">
                              <td class="tr-media-type" >
                                <g:if test="${media.idMediaType} == 1"><i class="glyphicon glyphicon-film"></i></g:if>
                                <g:else><i class="glyphicon glyphicon-picture"></i></g:else>
                              </td>
                              <td class="tr-media-name" ><a data-toggle="tooltip" title="${media.url}">${media.name}</a></td>
                              <td class="tr-media-add"><button class="btn btn-link btn-xs" onclick="addToEditPlaylist('${media.id}', '${media.name}', ${media.idMediaType}, '${media.url}')"><i class="glyphicon glyphicon-plus"></i></button></td>
                            </tr>
                          </g:each>

                        </tbody>
                      </table>
                    </div>
                  </div>
                </section>

            </div><!--end content-->
          </div>
        </div><!--end modal-->        
        <!--Modal edit playlist-->


        <!--Modal schedule playlist-->
        <div class="modal fade" id="modal-calendar" tabindex="-1" role="dialog" data-backdrop="static">
          <div id="modal-calendar-schedule" class="modal-dialog">
            <div class="modal-content">
              <form id="form-schedule"> 
                <input id="hiddenSchedule" type="hidden" />             
                <div class="playlist-header">
                  <div class="header-modal-schedule">
                    <h4>KFC</h4>
                  </div>
                  <div class="header-delete-schedule">
                    <button id="btn-del-schedule" type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#del-schedule-modal">Delete Schedule</button>
                  </div>                      
                </div><!--end form header-->

                <hr/>

                <div class="table-modal">
                  <table class="table table-striped table-playlist">
                    <tbody id="body-schedule-row">

                    </tbody>
                  </table>
                </div><!--end playlist table-->
                <div class="playlist-input-footer">
                  <input type="hidden" id="playlistIdSchedule"/>
                  <div class="desc-input">
                    <div class="form-group">
                      <input type="text" id="desc-schedule" class="form-control input-sm input-readonly"  placeholder="Description" readonly/>               
                    </div>
                  </div>                      
                  <div class="date-input">
                    <div class="inner-addon right-addon date1">
                        <i class="glyphicon glyphicon-calendar"></i>
                        <input type="text" id="txtScheduleStart" name="startDate" class="form-control input-sm txtStart" placeholder="Start Date"/>
                    </div>
                    <div class="inner-addon right-addon date2">
                        <i class="glyphicon glyphicon-calendar"></i>
                        <input type="text" id="txtScheduleEnd" name="endDate" class="form-control input-sm txtEnd" placeholder="End Date"/>
                    </div>
                  </div><!--end date-input-->
                  <div class="playlist-footer">
                      <div class="summary-playlist">
                        <span class="sum-media">0</span>&nbsp;media(s)
                      </div>
                    <div class="playlist-footer-btn">
                      <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                      <button type="submit" class="btn btn-success" >Save</button>
                    </div>
                  </div><!--end playlist footer-->
                </div>
              </form>    
            </div><!--end content-->
          </div>
        </div><!--end modal-->        
        <!--Modal schedule playlist-->        


        <div class="modal fade" id="del-playlist-modal" tabindex="-1" role="dialog" data-backdrop="static">
          <div id="modal-dialog-del-media" class="modal-dialog" role="document">
            <div class="modal-content" id="modal-content-del-media">
            <div class="row">
              <div class="col-lg-12">
                <div class="del-header">
                  <h3>Delete Confirmation</h3>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <br/>
                This action will delete this <span class="modal-underline">playlist</span>
                and <span class="modal-underline">all using in schedule</span>.
                <br/>
                Are you sure to delete it?
              </div>
            </div>
            <div id="btn-del-zone" class="row">
              <div class="col-lg-6">
                <button type="button" class="btn btn-gray btn-block" data-dismiss="modal">Cancel</button>
              </div>
              <div class="col-lg-6">
                <button type="button" class="btn btn-danger btn-block" onclick="deletePlaylist()">Delete</button>
              </div>
            </div>
            </div>
          </div>
        </div><!--end delete modal-->

        <div class="modal fade" id="del-schedule-modal" tabindex="-1" role="dialog" data-backdrop="static">
          <div id="modal-dialog-del-media" class="modal-dialog" role="document">
            <div class="modal-content" id="modal-content-del-media">
            <div class="row">
              <div class="col-lg-12">
                <h3>Delete Confirmation</h3>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                <br/>
                This action will delete this <span class="modal-underline">Schedule</span>.
                <br/>
                Are you sure to delete it?
              </div>
            </div>
            <div id="btn-del-zone" class="row">
              <div class="col-lg-6">
                <button type="button" class="btn btn-gray btn-block" data-dismiss="modal">Cancel</button>
              </div>
              <div class="col-lg-6">
                <button type="button" class="btn btn-danger btn-block" onclick="deleteSchedule()">Delete</button>
              </div>
            </div>
            </div>
          </div>
        </div><!--end delete modal-->        


        <!--                    Modal                    -->

      </div>

</body>
</html>
