class UrlMappings {

	static mappings = {

        /*"/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }*/
        "/$action?/$id?" (controller : 'home')
        "/" (controller : 'home', action : "libary")
        "500" (view:'/error')
        "404" (controller : 'home')
	}
}
